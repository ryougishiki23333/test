import { store } from '@ice/stark-data'
const mainAppStore = {
  state: {
    // 获取主应用传过来的state
    bpmUser: store.get('bpmUser'),
    company: store.get('company'), // 获取公司信息
    aUser: store.get('aUser'), // 获取A用户信息
    user: store.get('user'), // 获取用户信息
    // 获取b登录信息
    bToken: store.get('bToken'),
    bTenantId: store.get('bTenantId'),
    // 获取a登录信息
    aToken: store.get('aToken'),
    aTenantId: store.get('aTenantId'),
    isAgencySystem: store.get('isAgencySystem'), // 是否代理机构，兼容旧应用
     // 设置模式
    userType: store.get('userType'),
    userCenter: store.get('userCenter'),
    // 获取项目id
    projectId: store.get('projectId'),

  },
  mutations: {
    mainStateChange: (state, params) => {
      state.mainState = params
    },
    mainStoreChange: (state, params) => {
      state.mainAppStore = params
    }
  },
  actions: {
    mainStateChange (context, params) {
      context.commit('mainStateChange', params)
    },
    mainStoreChange (context, params) {
      context.commit('mainStoreChange', params)
    }
  }
}

/*
子应用触发改变主应用的state实例
...mapState({
  mainAppStore: state => state.mainAppStore.mainAppStore
})

this.mainAppStore.dispatch('microPreviewDataChange', paramsObj)
*/
export default mainAppStore
