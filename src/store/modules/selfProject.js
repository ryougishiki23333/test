import moment from 'moment'

// 自主立项数据
const selfProject = {
  state: {
    currentStep: 'FirstStepForm',
    filters: ''
  },
  mutations: {
    SET_CURRENT_STEP: (state, step) => {
      state.currentStep = step
    },
    SET_Filters: (state, searchValue = {}) => {
      const { projectNum, projectStatus, projectName, auditState, mode, agencyId, acceptId = '' } = searchValue
      let filters = ''
      filters += projectName ? ` and projectName.contains("${projectName}")` : ''
      filters += projectNum ? ` and projectNum.contains("${projectNum}")` : ''
      // 项目状态判断
      const status = projectStatus ? projectStatus.split(',') : ''
      if (status[0] === '3') {
        // 报名报价阶段等条件
        filters += projectStatus ? ` and state == "${status[0]}"` : ''
        const currentTime = moment().locale('zh-cn').format('YYYY-MM-DD HH:mm:ss')
        const timeFilter = {
          '1': ` and signUpStartTime > "${currentTime}"`, // 即将报名
          '2': ` and signUpStartTime < "${currentTime}" and signUpEndTime > "${currentTime}"`, // 正在报名
          '3': ` and ((signUpEndTime < "${currentTime}" and quotationStartTime > "${currentTime}") or (signUpEndTime < "${currentTime}" and secQuoteStartTime > "${currentTime}"))`, // 即将报价
          '4': ` and ((quotationStartTime < "${currentTime}" and quotationEndTime > "${currentTime}") or (secQuoteStartTime < "${currentTime}" and secQuoteEndTime > "${currentTime}"))`, // 正在报价
          '5': ` and ((quotationEndTime < "${currentTime}"  and secQuoteStartTime == null) or (secQuoteStartTime != null and secQuoteEndTime < "${currentTime}"))`, // 报价结束
          'e2': ` and signUpStartTime < "${currentTime}" and quotationStartTime > "${currentTime}"` // 审核报名阶段
        }
        filters += timeFilter[status[1]]
      } else if (status[0] === 'OR') {
        let tem = Object.assign([], status)
        tem.splice(0, 1)
        tem = tem.join(`" OR state == "`)
        filters += ` and (state == "${tem}")`
      } else {
        if (status[0] !== '-1') {
          filters += projectStatus ? ` and state == "${status[0]}"` : ''
        } else {
          filters += ` and state > 0`
        }
      }
      // 审核状态判断
      if (typeof auditState === 'undefined') {
        filters += ` and auditState != "0"`
      } else if (auditState === null) {
        filters += ` and auditState == null`
      } else if (auditState.toString().indexOf('OR') !== -1) {
        const fil = auditState.split('OR').join(` OR auditState == `)
        filters += ` and (auditState == ${fil})`
      } else if (auditState.toString().indexOf('!') !== -1) {
        const fil = auditState.split('!').join(` and auditState != `)
        filters += ` and (auditState != ${fil})`
      } else {
        filters += ` and auditState == "${auditState}"`
      }
      // 项目类型
      if (typeof mode !== 'undefined') {
        filters += ` and mode == ${mode}`
      }
      // 代理机构判断
      if (agencyId !== '') {
        filters += ` and agencyId == "${agencyId}"`
      }
      if (acceptId !== '') {
        filters += ` and acceptId == "${acceptId}"`
      }
      state.filters = filters.slice(5)
    }
  }
}

export default selfProject
