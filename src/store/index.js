import Vue from 'vue'
import Vuex from 'vuex'

import mainAppStore from './modules/mainAppStore'
import getters from './getters'
import selfProject from './modules/selfProject'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  modules: {
    mainAppStore,
    selfProject,
  },
  actions: {
  },
  getters,
  mutations: {
  }
})