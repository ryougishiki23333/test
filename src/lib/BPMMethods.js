import { camundaProcessStart, camundaProcessCirculate, camundaProcessGetInstanceByBusinessKeyLike } from '@/api/BPM/process'
import { camundaTaskGetTasksByProcessInstanceId, camundaTaskClaim } from '@/api/BPM/task'
import { camundaHistoryGetVariableHistory } from '@/api/BPM/history'

export async function startProcess (processKey, businessKey, form) {
  try {
    if (!(processKey && businessKey && form)) throw new Error('参数缺失')
    const params = { businessKey, form }
    const { result } = await camundaProcessStart(processKey, params)
    return result
  } catch (error) {
    return error
  }
}
export async function processCirculate (taskId, form = {}) {
  try {
    if (!taskId) throw new Error('参数缺失')
    const { result } = await camundaProcessCirculate(taskId, form)
    return result
  } catch (error) {
    console.log(error)
    return error
  }
}
export async function getTaskByProcessInstanceId (processInstanceId) {
  try {
    const { result } = await camundaTaskGetTasksByProcessInstanceId(processInstanceId)
    return result.items
  } catch (error) {
    console.log(error)
  }
}
export async function getVariableByInstanceId (instanceId) {
  if (!instanceId) return
  try {
    const params = {
      instanceId,
      pageNum: 1,
      pageSize: 1000
    }
    const { result } = await camundaHistoryGetVariableHistory(params)
    console.log(result.items)
    const res = {}
    if (result.items.length) {
      result.items.forEach(v => {
        res[v.name] = v.value
      })
    }
    return res
  } catch (error) {
    return error
  }
}
export async function claimTask (taskId, userId) {
  try {
    if (!(taskId && userId)) throw new Error('参数缺失')
    const { result } = await camundaTaskClaim(taskId, userId)
    return result
  } catch (error) {
    return error
  }
}
export async function getInstanceByBusinessKey (businessKey) {
  try {
    if (!businessKey) throw new Error('参数缺失')
    const { result } = await camundaProcessGetInstanceByBusinessKeyLike({ businessKey, pageNum: 1, pageSize: 100 })
    return result.items
  } catch (error) {
    return error
  }
}
// 这个是原始版本，用户注册没有用BUser的ID
export default class BPMMethods {
  constructor (BPMAPIs) {
    this.BPMAPIs = BPMAPIs // 传入接口
  }

  // --------------process----------------
  /**
   * 开始流程-（参数都必传）
   * @param {String} processKey 流程ID
   * @param {String} businessKey businesskey（一般为：JSON.stringify({项目id，项目标题})）
   * @param {Object} form 附加信息
   * @returns {Object} 流程实例信息
   * */
  async startProcess (processKey, businessKey, form) {
    try {
      if (!(processKey && businessKey && form)) throw new Error('参数缺失')
      const params = { businessKey, form }
      const { result } = await camundaProcessStart(processKey, params)
      return result
    } catch (error) {
      return error
    }
  }

  /**
   * 流程流转
   * @param {String} taskId startProcess返回的流程实例Id(processInstanceId)
   * @param {Object} form 附加信息（遇到判断的流程节点时，form中的字段作为流程节点判断依据）
   * @returns
   */
  async processCirculate (taskId, form = {}) {
    try {
      if (!taskId) throw new Error('参数缺失')
      const { result } = await camundaProcessCirculate(taskId, form)
      return result
    } catch (error) {
      console.log(error)
      return error
    }
  }

  /**
   * 根据businessKey获取流程实例Id
   * @param {String} businessKey
   * @returns {string} InstanceId（实例Id）
   */
  async getInstanceByBusinessKey (businessKey) {
    try {
      if (!businessKey) throw new Error('参数缺失')
      const { result } = await this.BPMAPIs.process.camundaProcessGetInstanceByBusinessKeyLike({ businessKey, pageNum: 1, pageSize: 100 })
      return result.items
    } catch (error) {
      return error
    }
  }

  // ----------------taks------------------
  /**
   * 用户认领流程
   * @param {String} taskId startProcess返回的流程实例Id(processInstanceId)
  */
  async claimTask (taskId, userId) {
    try {
      if (!(taskId && userId)) throw new Error('参数缺失')
      const { result } = await camundaTaskClaim(taskId, userId)
      return result
    } catch (error) {
      return error
    }
  }

  /**
   * 获取任务列表
   * @param {String} businessKey businesskey（一般为：JSON.stringify({项目id，项目标题})）
   * @param {String} tenantId
   * @param {String} userId
   * @returns
   */
  async getTaskList (businessKey, tenantId, userId) {
    try {
      const { result } = await this.BPMAPIs.task.camundaTaskBusinessKey(businessKey, tenantId, userId)
      return result
    } catch (error) {
      return error
    }
  }

  // 获取流程实例信息
  async getTaskByProcessInstanceId (processInstanceId) {
    try {
      const { result } = await camundaTaskGetTasksByProcessInstanceId(processInstanceId)
      return result.items
    } catch (error) {
      console.log(error)
    }
  }
  // ----------------user-----------------
  // 获取单个用户信息
  async getUserMsg (userName) {
    const { result } = await this.BPMAPIs.user.camundaUserGet({ userId: userName })
    return result
  }
  // 注册用户
  async registerUser (userMsg) {
    try {
      if (!userMsg.name) throw (new Error('缺失用户名'))
      // 增加检测userMsg.name是否emoji，如果有，去除String.split()后的'\u'再拼接
      // 重新编码userMsg.name
      const params = {
        attempts: 0,
        dbPassword: '',
        email: userMsg?.emailAddress,
        firstName: userMsg.name,
        id: userMsg.BPM_USER_ID,
        lastName: userMsg.name,
        lockExpirationTime: '',
        password: userMsg.name,
        persistentState: {
          firstName: userMsg.name,
          lastName: userMsg.name,
          password: userMsg.name,
          salt: '',
          email: userMsg?.emailAddress
        },
        salt: ''
      }
      const { result } = await this.BPMAPIs.user.camundaUserInsert(params)
      console.log('用户新增成功')
      return result
    } catch (error) {
      return error
    }
  }

  /**
   * 获取当前用户的所有租户信息
   * @param {String} userId
   * @returns {Array}
   */
  async getCurrentUserTenantID (userId) {
    try {
      if (!userId) throw (new Error('缺少参数'))
      const { result } = await this.BPMAPIs.user.camundaTenantUserGetList(userId)
      return result.items
    } catch (error) {
      return error
    }
  }
  // ----------------tenant-----------------
  // 获取单个租户信息
  async getTenantMsg (tenantId) {
    try {
      const params = {
        id: `${tenantId}`
      }
      const { result } = await this.BPMAPIs.user.camundaTenantGetList(params)
      return result
    } catch (error) {
      return error
    }
  }

  // 新增租户
  async addTenant (tenantMsg) {
    try {
      if (!(tenantMsg?.id && tenantMsg?.name)) throw (new Error('数据缺失'))
      const params = {
        id: `${tenantMsg.id}`,
        name: tenantMsg.name,
        persistentState: {
          name: tenantMsg.name
        }
      }
      await this.BPMAPIs.user.camundaTenantInsert(params)
      console.log('新增租户成功')
      return true
    } catch (error) {
      return error
    }
  }

  /**
   * 绑定用户租户
   * @param {String} userId
   * @param {String} tenantId
   * @returns
   */
  async bindUserTenant (userId, tenantId) {
    try {
      const params = `?userId=${userId}&tenantId=${tenantId}`
      const { result } = await this.BPMAPIs.user.camundaUserTenantBind(params)
      return result
    } catch (error) {
      return error
    }
  }

  // ----------------group-----------------
  // 绑定用户群组
  async bindUserGroup (userId, groupId) {
    try {
      const { result } = await this.BPMAPIs.user.camundaUserBroupBind(userId, groupId)
      return result
    } catch (error) {
      return error
    }
  }

  // ----------------history-----------------
  // 通过实例ID获取变量
  async getVariableByInstanceId (instanceId) {
    if (!instanceId) return
    try {
      const params = {
        instanceId,
        pageNum: 1,
        pageSize: 1000
      }
      const { result } = await camundaHistoryGetVariableHistory(params)
      console.log(result.items)
      const res = {}
      if (result.items.length) {
        result.items.forEach(v => {
          res[v.name] = v.value
        })
      }
      return res
    } catch (error) {
      return error
    }
  }

  // 获取流程历史
  async getHistoryByInstanceId (instanceId) {
    try {
      const params = {
        instanceId,
        pageNum: 1,
        pageSize: 1000
      }
      const { result } = await this.BPMAPIs.history.camundaHistoryGetProcessProgress(params)
      return result
    } catch (error) {
      return error
    }
  }
}
