import spin from './spin.vue'

let $vm = ''

const spinInstaller = {
  vm: {},
  install (Vue) {
    if (!$vm) {
      const SpinPlugin = Vue.extend(spin)

      $vm = new SpinPlugin({
        el: document.createElement('div')
      })

      document.body.appendChild($vm.$el)
    }
    Object.defineProperties(Vue.prototype, {
      $spin: {
        get: function get () {
          return $vm
        }
      }
    })
  }
}

export default spinInstaller
