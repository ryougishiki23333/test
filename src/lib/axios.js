import Vue from 'vue'
import axios from 'axios'
let message = null
import('ant-design-vue').then(module => message = module.message)
import {store} from '@ice/stark-data'
import {ACCESS_TOKEN, A_ACCESS_TOKEN, TENANT_ID, A_TENANT_ID} from '@/store/mutation-types'

const {ifGlobalUrl} = require('../globalConfig')
const urlConfig = window.urlConfig

const isMedical = store.get('userType') === 'medical'

let emptyUrl, middlewareUrl, baseUrl, platformUrl, userCenterUrl, QuoteURL, zjkUrl, fileUrl, MessageUrl, EvaluateUrl, middleUrl,
    sqliteUrl, finacialUrl, newProjectUrl;

if (ifGlobalUrl) {
  // 没有前缀路径
  emptyUrl = ''

  // 中间件模块
  middlewareUrl = urlConfig.requests.middlewareUrl

  // 项目接口 5003
  baseUrl = isMedical ? urlConfig.requests.medicalUrl : urlConfig.requests.projectUrl

  // 平台信息获取接口 5002
  platformUrl = isMedical ? urlConfig.requests.medicalUrl : urlConfig.requests.platformUrl

  // 用户中心接口 5001
  userCenterUrl = isMedical ? urlConfig.requests.medicalUrl : urlConfig.requests.userCenterUrl

  // 报价中心
  QuoteURL = 'https://api.choicelink.cn/OnLineQuoted'

  // 专家库 5008
  zjkUrl = urlConfig.requests.zjkUrl

  // 文件上传 5007
  fileUrl = urlConfig.requests.fileUrl

  // 消息中心 5006
  MessageUrl = urlConfig.requests.messageUrl

  // 线上评标
  EvaluateUrl = 'https://api.choicelink.cn/OnlineEvaluation'

  // 中间件模块（医疗器械）
  middleUrl = isMedical ? urlConfig.requests.middlewareUrl + '/medical' : urlConfig.requests.middlewareUrl

  // sqlite模块
  sqliteUrl = isMedical ? urlConfig.requests.middlewareUrl + '/medical/sqlite' : urlConfig.requests.middlewareUrl + '/sqlite'

  // 财务中心
  finacialUrl = 'https://api.choicelink.cn/NewFinancial'

  // 项目接口（新）
  newProjectUrl = isMedical ? urlConfig.requests.medicalNewProjectUrl : urlConfig.requests.newProjectUrl

} else {
  // 没有前缀路径
  emptyUrl = ''

  // 中间件模块
  middlewareUrl = process.env.NODE_ENV === 'production' ? 'https://middleware.choicelink.cn' : 'https://middleware.choicelink.cn'

  // 项目接口 5003
  baseUrl = process.env.NODE_ENV === 'production' ? `https://api.choicelink.cn/${isMedical ? 'Association' : 'OnLineProject'}` : `https://api.choicelink.cn/${isMedical ? 'Association' : 'OnLineProject'}`

  // 平台信息获取接口 5002
  platformUrl = process.env.NODE_ENV === 'production' ? `https://api.choicelink.cn/${isMedical ? 'Association' : 'PlatformCenter'}` : `https://api.choicelink.cn/${isMedical ? 'Association' : 'PlatformCenter'}`

  // 用户中心接口 5001
  userCenterUrl = process.env.NODE_ENV === 'production' ? 'https://api.choicelink.cn/UserCenter' : 'https://api.choicelink.cn/UserCenter'

  // 报价中心
  QuoteURL = process.env.NODE_ENV === 'production' ? 'https://api.choicelink.cn/OnLineQuoted' : 'https://api.choicelink.cn/OnLineQuoted'

  // 专家库 5008
  zjkUrl = process.env.NODE_ENV === 'production' ? 'https://api.choicelink.cn/OnLineZjkProject' : 'https://api.choicelink.cn/OnLineZjkProject'

  // 文件上传 5007
  fileUrl = process.env.NODE_ENV === 'production' ? 'https://api.choicelink.cn/OnLineUpload' : 'https://api.choicelink.cn/OnLineUpload'

  // 消息中心 5006
  MessageUrl = process.env.NODE_ENV === 'production' ? 'https://api.choicelink.cn/OnlineMessages' : 'https://api.choicelink.cn/OnlineMessages'

  // 线上评标
  EvaluateUrl = process.env.NODE_ENV === 'production' ? 'https://api.choicelink.cn/OnlineEvaluation' : 'https://api.choicelink.cn/OnlineEvaluation'

  // 中间件模块
  middleUrl = process.env.NODE_ENV === 'production' ? `https://middleware.choicelink.cn${isMedical ? '/medical' : ''}` : `https://middleware.choicelink.cn${isMedical ? '/medical' : ''}`

  // sqlite模块
  sqliteUrl = process.env.NODE_ENV === 'production' ? `https://middleware.choicelink.cn${isMedical ? '/medical/sqlite' : '/sqlite'}` : `https://middleware.choicelink.cn${isMedical ? '/medical/sqlite' : '/sqlite'}`

  // 财务中心
  finacialUrl = process.env.NODE_ENV === 'production' ? 'https://api.choicelink.cn/NewFinancial' : 'https://api.choicelink.cn/NewFinancial'

  // 项目接口（新）
  newProjectUrl = process.env.NODE_ENV === 'production' ? `https://middleware.choicelink.cn/${isMedical ? 'medical/onlineProject' : 'ordinary/onlineProject'}` : `https://middleware.choicelink.cn/${isMedical ? 'medical/onlineProject' : 'ordinary/onlineProject'}`

}

// 创建请求
function newRequest (url, authToken, tId = '') {
  // 创建 axios 实例
  const serve = axios.create({
    baseURL: url, // api base_url
    timeout: 60000 // 请求超时时间
  })
  const err = (error) => {
    if (error.response) {
      const data = error.response.data
      const token = authToken === 'token' ? store.get('bToken') : store.get('aToken')
      if (error.response.status === 403) {
        notification.error({
          message: 'Forbidden',
          description: data.message
        })
      }
      if (error.response.status === 401 && !(data.result && data.result.isLogin)) {
        notification.error({
          message: 'Unauthorized',
          description: 'Authorization verification failed'
        })
        if (token) {
          store.dispatch('Logout').then(() => {
            setTimeout(() => {
              window.location.href = webUrl.userCenter
              // window.location.reload()
            }, 1500)
          })
        }
      }
    }
    return Promise.reject(error)
  }
  // request interceptor
  serve.interceptors.request.use(config => {
    let token = ''
    if (authToken !== 'noToken') {
      token = authToken === 'token' ? store.get('bToken') : store.get('aToken')
    }
    const tenantId = store.get('bTenantId')
    if (token) {
      config.headers['Authorization'] = `Bearer ${token}` // 让每个请求携带自定义 token 请根据实际情况自行修改
    }
    if (tId === 'aTenantId') {
      config.headers['Abp.TenantId'] = Vue.ls.get(A_TENANT_ID) || '1' // 用户中心租户
    } else if (tenantId && tId !== 'noId') {
      config.headers['Abp.TenantId'] = tenantId // 平台租户
    }

    // config.url = config.baseURL + config.url
    return config
  }, err)
  // response interceptor
  serve.interceptors.response.use((response) => {
    return response.data
  }, err)
  return serve
}

const emptyService = newRequest(emptyUrl, 'noToken', 'noId')
const middlewareService = newRequest(middlewareUrl, 'token')
const PlatformService = newRequest(platformUrl, 'token')
const ProjectService = newRequest(baseUrl, 'token')
const userCenterService = newRequest(userCenterUrl, 'Atoken', 'aTenantId')
const quoteService = newRequest(QuoteURL, 'token')
const MiddleService = newRequest(middleUrl, 'token')
const SqliteService = newRequest(sqliteUrl, 'token')

const fileService = newRequest(fileUrl, 'Atoken')
const zjkService = newRequest(zjkUrl, 'token')
const MessageService = newRequest(MessageUrl, 'Atoken', 'aTenantId')
const EvaluateService = newRequest(EvaluateUrl, 'token')
const FinacialService = newRequest(finacialUrl, 'token')
const newProjectService = newRequest(newProjectUrl, 'token')

// 地区三级联动用这个接口
const SqliteAxios = newRequest(`${middlewareUrl}/sqlite`, 'token')
// 'http://159.75.211.223:8086/sqlite'
// 'http://http://42.193.254.79:9001/sqlite'
export {
  emptyService,
  middlewareService,
  PlatformService,
  ProjectService,
  userCenterService,
  quoteService,
  MiddleService,
  SqliteService,
  fileService,
  zjkService,
  MessageService,
  EvaluateService,
  FinacialService,
  newProjectService,

  SqliteAxios
}
