/**
 * ******************
 * ** 注册 && 绑定 **
 * ******************
 */
 import Vue from 'vue'
 import storage from "@/store"
 import { store } from '@ice/stark-data'
 import {
    camundaTenantGetList,
    camundaTenantInsert,
    camundaUserGetList,
    camundaUserInsert,
    camundaUserTenantBind,
    camundaTenantUserGetList,
    camundaGroupUserGetList,
    camundaGroupGetList,
    camundaGroupInsert,
    camundaGroupTenantGetList,
    camundaTenantBind,
    camundaUserBroupBind,
    camundaUserGroupUnbind,
    SqliteBacklogGet
 } from "@/api/bpm"
 import { getBUserRoles } from '@/api/platformCenter'
 import { TENANT_ID } from '@/store/mutation-types'
 
 // 检查租户创建情况
 async function checkTenantRegister() {
    const tenantId = Vue.ls.get(TENANT_ID)
    // const bTenantMsg = storage.getters.userInfo.tenant
    //  检查vuex中是否已有BPM租户信息
    const VUEXtenantInfo = storage.state.bpmUser.tenantInfo
    if (VUEXtenantInfo && VUEXtenantInfo.id === tenantId) return
    // 检测租户是否创建
    const { result: tenantRes } = await camundaTenantGetList({ id: tenantId })
    console.log(tenantRes)
    // 判断是否已创建租户
    let tenantMsg
    if (!tenantRes.items.length) {
        // 无-创建租户
        const { success } = await camundaTenantInsert({
            id: `${bTenantMsg.id}`,
            name: bTenantMsg.name,
        })
        if (success) {
            const { result: tenantRes } = await camundaTenantGetList({ id: tenantId })
            tenantMsg = tenantRes.items[0]
        } else {
            throw new Error("BPM租户创建失败")
        }
    } else {
        // 已有租户
        tenantMsg = tenantRes.items[0]
    }
    // 保存租户信息
    storage.dispatch("SET_BPM_TENANT", tenantMsg)
    console.log("BPM租户已创建")
 }
 
 // 检查用户创建情况
 async function checkUserRegister() {
    // const BUserMsg = storage.getters.userInfo.user
    const tenantId = Vue.ls.get(TENANT_ID) + ""
    const BPMUserId = `${tenantId}${BUserMsg.id}`
    // 检查vuex中是否已有BPM用户信息
    // const VUEXuserInfo = storage.state.bpmUser.userInfo
    if (VUEXuserInfo && VUEXuserInfo.id === BPMUserId) return
    // 检测租户是否创建
    const { result: BUserRes } = await camundaUserGetList({ id: BPMUserId }) // BUserID对应BPM的用户ID
    // 判断是否已创建租户
    let bUserMsg
    if (!BUserRes.items.length) {
        // 无-创建用户
        const params = {
            attempts: 0,
            id: BPMUserId,
            email: BUserMsg.emailAddress,
            firstName: BUserMsg.name,
            lastName: BUserMsg.userName,
            password: BPMUserId,
        }
        const { success } = await camundaUserInsert(params)
        if (success) {
            const { result: BUserRes } = await camundaUserGetList({ id: BPMUserId })
            bUserMsg = BUserRes.items[0]
        } else {
            throw new Error("BPM用户创建失败")
        }
    } else {
        // 已有租户
        bUserMsg = BUserRes.items[0]
    }
    // 保存用户信息
    storage.dispatch("SET_BPM_USER", bUserMsg)
    console.log("BPM账户已创建")
 }
 
 // 绑定用户租户
 async function bindUserTenant() {
    if (!storage.state.bpmUser.userInfo || !storage.state.bpmUser.tenantInfo) return
    const BUserMsg = storage.getters.userInfo.user
    const tenantId = Vue.ls.get(TENANT_ID) + ""
    const BPMUserId = `${tenantId}${BUserMsg.id}`
    // 查询是否已绑定
    const { result: userTenantList } = await camundaTenantUserGetList(BPMUserId)
    let IfBind = false // 是否已绑定
    for (let i = 0; i < userTenantList.items.length; i++) {
        const tenant = userTenantList.items[i]
        if (tenant.id === tenantId) {
            IfBind = true
            break
        }
    }
    // 尚未绑定
    if (!IfBind) {
        const params = `?userId=${BPMUserId}&tenantId=${tenantId}`
        await camundaUserTenantBind(params)
    }
    console.log("BPM用户租户已绑定")
 }

 // 查询群组是否创建
 async function checkGroupRegister (role, tenantId) {
    const {result: {items}} = await camundaGroupGetList({
        name: role,
        pageSize: 1,
        type: tenantId
    })
    if (items.length > 0) {
        return items[0].id
    } else {
        // 查询当前角色在用户中心b的id
        const {result: {items: BRole}} = await getBUserRoles({
            where: `name.contains("${role}")`,
            SkipCount: 0,
            MaxResultCount: 1
        })
        // 加上特定标识，避免冲突
        // 创建群组
        camundaGroupInsert({
            id: `whole${BRole[0].id}`,
            name: role,
            type: tenantId
        })
        return `whole${BRole[0].id}`
    }
 }

 // 检测群组是否绑定租户
 async function checkGroupBind (role, tenantId, groupId) {
    const {result: {items: groups}} = await camundaGroupTenantGetList({
        pageSize: 100,
        tenantId: tenantId
    })
    const ifBind = groups.findIndex(group => group.name === role) !== -1
    if (!ifBind) {
        await camundaTenantBind({
            groupId,
            tenantId
        })
    }
 }
 
// 获取当前用户group信息
 async function getCurrentUserGroupInfo() {
    // const BUserRoles = storage.getters.userInfo.roleNames
    const BPMUser = storage.state.bpmUser
    // 查询已绑定群组
    const { result: { items: BindRoles } } = await camundaGroupUserGetList({
        // userId: BPMUser.userInfo.id
    })
    // 未绑定群组角色
    const UnBindRoles = BUserRoles.filter(role => {
        return BindRoles.findIndex(bRole => role === bRole.name) === -1
    })
    if (UnBindRoles.length > 0) {
        UnBindRoles.forEach(async unBind => {
            // 检测群组是否创建
            const GroupId = await checkGroupRegister(unBind, BPMUser.tenantInfo.id)
            // 检测群组是否绑定租户
            await checkGroupBind(unBind, BPMUser.tenantInfo.id, GroupId)
            // 用户绑定群组
            await camundaUserBroupBind({
                groupId: GroupId,
                // userId: BPMUser.userInfo.id
            })
        })
    }
    // 解绑取消的角色
    const needUnBind = BindRoles.filter(role => {
        return BUserRoles.findIndex(bRole => role.name === bRole) === -1
    })
    if (needUnBind.length > 0) {
        needUnBind.forEach(unBind => {
            camundaUserGroupUnbind({
                groupId: unBind.id,
                // userId: BPMUser.userInfo.id
            })
        })
    }
 }
 
 // 检查用户注册 及 关联租户
 export async function userCheckRegisterAndCombineTenant() {
    // 检测租户是否创建
    await checkTenantRegister()
    // 检测用户是否创建
    await checkUserRegister()
    // 绑定用户租户
    await bindUserTenant()
    // 获取当前用户group信息
    await getCurrentUserGroupInfo()
    store.set('bpmUser', storage.state.bpmUser)
 }

 // 查询流程待办数量
 export async function getFlowTodoCount () {
     if (!store.get('bpmUser')) {
        return
     }
    const todoCount = await SqliteBacklogGet({
      filter: `type == "公告审核" and status == "0" and handleUserId == "${store.get('bpmUser').userInfo.id}"`
    })
    storage.dispatch("SET_TOTAL_COUNT", todoCount.result.totalCount)
    storage.dispatch("SET_DETAIL_COUNT", {
        noticeFlow: todoCount.result.totalCount
    })
  }
 