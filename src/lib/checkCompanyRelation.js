// new(这个要给钱的，不便宜啊)
import axios from 'axios'

/**
 * 判读传入的公司名称是否有关联
 * @param {Array} companyNameGroup
 * @returns {Boolean}
 */
// eslint-disable-next-line no-unused-vars
async function getCompanyMatch (companyNameGroup = []) {
  if (companyNameGroup.length < 2) {
    return Error('传参错误!')
  }
  const baseUrl = 'https://siteserver.choicelink.cn'
  const params = {
    namefrom: companyNameGroup[0],
    nameto: companyNameGroup[1]
  }
  const { data } = await axios({
    url: `${baseUrl}/api/OpenTyc/shortPath_V2`,
    method: 'get',
    params
  })
  return data
}

/**
 * @param {Array} companyData
 * @returns { msg: [Object], record: [Array] }
 * 将有关系的公司对应好，返回直接渲染
 */
export async function matchCompanyRelation (companyData) {
  if (!Array.isArray(companyData)) {
    return {
      msg: '传参错误!'
    }
  }
  try {
    const record = []
    // 处理替换原始公司信息英文符号为中文符号
    companyData.map(v => {
      v.companyName = v.companyName.replace('(', '（')
      v.companyName = v.companyName.replace(')', '）')
    })
    // 将公司名称分组对应好
    const companyGroup = []
    for (let i = 0; i < companyData.length; i++) {
      for (let j = 0; j < i; j++) {
        companyGroup.push([companyData[i].companyName, companyData[j].companyName])
      }
    }
    // Ajax
    for (let i = 0; i < companyGroup.length; i++) {
      const res = await getCompanyMatch(companyGroup[i])
      console.log(res)
      // const str = JSON.stringify(res?.coordinate) + ''
      // if (str !== 'undefined' && str !== 'null' && str !== '{}') {
      //   const mainStr = companyGroup[i].join(' 跟 ')
      //   record.push(`${mainStr} 存在关联`)
      // }
      if (res.message && res.resultcode === 0) {
        const mainStr = companyGroup[i].join(' 跟 ')
        record.push(`${mainStr} 关联关系为 ${res.message}`)
      } else if (res.resultcode !== 0) {
        return { msg: '查询失败，请检查要查询的公司名称是否正确' }
      }
      if (i === companyGroup.length - 1) {
        return { msg: '查询成功', record }
      }
    }
  } catch (error) {
    console.log(error)
    return { msg: '查询失败，请检查要查询的公司名称是否正确' }
  }
}

// old（暂时不用）
// // 查找公司关联
// import axios from 'axios'
// const checkRelation = {
//   getCompanyIDByName: function (companyName) {
//     return axios.get(`https://sp0.tianyancha.com/search/suggestV2.json?key=${companyName}`)
//   },
//   getCompanyControlPersionMsgByCompanyID: function (companyID) {
//     return axios.get((process.env.NODE_ENV !== 'production' ? 'https://api.choicelink.cn/TYC' : '/tyc') + `/relation/getRelatedHuman.json?id=${companyID}`)
//   }
// }
// /**
//  * 传入参数需为Array；每条信息必须包含companyName属性
// */
// export async function matchCompanyRelation (companyData) {
//   if (!Array.isArray(companyData)) return { msg: '传入参数错误' }
//   try {
//     // const startTime = new Date().getTime()
//     // 处理替换原始公司信息英文符号为中文符号
//     companyData.map(v => {
//       v.companyName = v.companyName.replace('(', '（')
//       v.companyName = v.companyName.replace(')', '）')
//     })
//     // 获取天眼查公司ID
//     let reqList = []
//     companyData.forEach(v => {
//       reqList.push(checkRelation.getCompanyIDByName(v.companyName))
//     })
//     const TycCompanyData = await Promise.all(reqList)
//     // 将tyc公司ID插入本地数据
//     TycCompanyData.map(v => {
//       companyData.map(j => {
//         if (v.data.data[0]) {
//           if (v.data.data[0].comName === j.companyName || v.data.data[0].comName.includes(j.companyName)) {
//             j.TycID = v.data.data[0].id
//           }
//         }
//       })
//     })
//     // console.log('天眼查获取的公司数据', TycCompanyData)
//     // console.log('本地数据', companyData)
//     reqList = []
//     companyData.forEach(v => {
//       reqList.push(checkRelation.getCompanyControlPersionMsgByCompanyID(v.TycID))
//     })
//     // console.log('本地数据', companyData)
//     const TycPersionData = await Promise.all(reqList)
//     // 将tyc公司人数据插入本地数据
//     // eslint-disable-next-line no-useless-escape
//     const reg = new RegExp(/\?id\=\d+$/)
//     TycPersionData.forEach(v => {
//     // console.log(reg.exec(v.config.url)?.[0]?.slice(4))
//       companyData.map(j => {
//         if (reg.exec(v.config.url)[0]) {
//           if (j.TycID + '' === reg.exec(v.config.url)[0].slice(4)) {
//             j.persionData = v.data
//             // console.log('j.persionData', j.persionData)
//             // 在本地数据中插入Tyc公司人的数据字符串
//             let str = ''
//             v.data.forEach(k => { str += `${k.name}:${k.id},` })
//             j.persionStr = str
//           }
//         }
//       })
//     })
//     // 记录公司关联信息
//     const record = []
//     // 查找公司间是否有关联人员
//     for (let i = 0; i < companyData.length; i++) {
//       for (let j = i + 1; j < companyData.length; j++) {
//       // console.log(companyData[i], companyData[j])
//         if (companyData[i].persionData) {
//           for (let k = 0; k < companyData[i].persionData.length; k++) {
//           // console.log('匹配的人的str', companyData[j]?.persionStr)
//           // console.log('匹配的人的id', companyData[i]?.persionData[k].id)
//             if (companyData[j].persionStr.includes(companyData[i].persionData[k].id)) {
//               record.push(`${companyData[i].companyName}---${companyData[j].companyName}`)
//               break
//             }
//           }
//         }
//       }
//     }
//     // console.log('天眼查获取的公司人的数据', TycPersionData)
//     // console.log('本地数据', companyData)
//     // console.log('关联信息', record)
//     // console.log('耗时', new Date().getTime() - startTime)
//     return { msg: '查询成功', record }
//   } catch (error) {
//     // console.log(error)
//     return { msg: '查询失败，请检查要查询的公司名称是否正确' }
//   }
// }
