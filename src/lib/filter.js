import Vue from 'vue'
import moment from 'moment'
import 'moment/locale/zh-cn'
import { winBidRules } from './config'
moment.locale('zh-cn')

Vue.filter('NumberFormat', function (value) {
  if (!value) {
    return '0'
  }
  const intPartFormat = value.toString().replace(/(\d)(?=(?:\d{3})+$)/g, '$1,') // 将整数部分逢三一断
  return intPartFormat
})

Vue.filter('dayjs', function (dataStr, pattern = 'YYYY-MM-DD') {
  return moment(dataStr).format(pattern)
})

Vue.filter('moment', function (dataStr, pattern = 'YYYY-MM-DD HH:mm:ss') {
  if (!dataStr) return ''
  return moment(dataStr).format(pattern)
})

Vue.filter('projectState', function (state, info = {}) {
  let newState = state
  const stateProject = {
    '0': '待编制',
    '1': '文件编制完成',
    '2': '审核通过',
    '3,1': '即将开始',
    '3,2': '正在报名',
    '3,3': '即将报价',
    '3,4': '正在报价',
    '3,5': '报价结束',
    '3,7': '结果审核中',
    '3,6': '二次报价',
    '4': '结果确认',
    '5': '项目中止',
    '6': '项目失败',
    '7': '项目结项'
  }
  if (state === 3) {
    const { signUpStartTime, signUpEndTime, quotationStartTime, quotationEndTime, secQuoteStartTime, secQuoteEndTime } = info
    const applyTimeBegin = new Date(signUpStartTime).getTime()
    const applyTimeEnd = new Date(signUpEndTime).getTime()
    const uppriceTimeBegin = new Date(quotationStartTime).getTime()
    const uppriceTimeEnd = new Date(quotationEndTime).getTime()
    const secUppriceTimeBegin = new Date(secQuoteStartTime).getTime()
    const secUppriceTimeEnd = new Date(secQuoteEndTime).getTime()
    const currentTime = new Date().getTime()
    if (currentTime < applyTimeBegin) { // 即将报名
      newState = '3,1'
    } else if (applyTimeBegin < currentTime && currentTime <= applyTimeEnd) { // 正在报名
      newState = '3,2'
    } else if (applyTimeEnd < currentTime && currentTime <= uppriceTimeBegin) { // 即将报价
      newState = '3,3'
    } else if (uppriceTimeBegin < currentTime && currentTime <= uppriceTimeEnd) { // 正在报价
      newState = '3,4'
    } else if (secUppriceTimeBegin < currentTime && currentTime <= secUppriceTimeEnd) {
      newState = '3,6'
    } else {
      newState = info.auditState === '11' ? '3,7' : '3,5'
    }
  }
  return stateProject[newState]
})

Vue.filter('noticeType', function (value) {
  // 公告类型
  switch (value) {
    case '0':
      return '招标'
    case '1':
      return '更正'
    case '2':
      return '成交'
    case '3':
      return '补充'
    case '5':
      return '结果'
    case '6':
      return '失败'
  }
})

Vue.filter('priceRule', function (value) {
  // 报价类型
  switch (value) {
    case 1:
      return '不公开报价：在报价过程中，不公开报价供应商的公司名称及报价金额'
    case 2:
      return '半公开报价：在报价过程中，不公开报价供应商的公司名称，公开报价金额'
    case 3:
      return '全公开报价：在报价过程中，公开报价供应商的公司名称及报价金额'
  }
})

export function getWinbidRuleLabel (val) {
  const res = winBidRules.filter(v => v.value === val)[0] || {}
  return res.label || ''
}

Vue.filter('winbidRule', function (value) {
  // 成交方法
  return getWinbidRuleLabel(value)
})

Vue.filter('winbidRule-explain', function (value) {
  // 成交方法解释
  switch (value) {
    case '1':
      return '最低价成交法：所有供应商最终报价中报价最低者（若价格相同则以报价时间最早者），选定为成交候选人'
    case '2':
      return '最高价成交法：所有供应商最终报价中报价最高者(若价格相同则以报价时间最早者)，选定为成交候选人'
    case '3':
      return '平均价成交法：所有供应商最终报价中最接近报价平均值者(若价格相同则以报价时间最早者)，选定为成交候选人'
    case '4':
      return '次低价成交法：所有供应商最终报价中报价次低者(若价格相同则以报价时间最早者)，选定为成交候选人'
    case '5':
      return '综合评分法：所有供应商评审后获得综合评分最高者(若评分相同则以价格最低者)，选定为成交候选人'
  }
})

Vue.filter('state', function (value) {
  //  审核状态
  switch (value) {
    case 0:
      return '立项'
    case 1:
      return '编制完成'
    case 2:
      return '审核通过'
    case 3:
      return '已发布'
    case 4:
      return '确认结果'
    case 5:
      return '中止'
    case 6:
      return '失败'
    case 7:
      return '结项'
  }
})
