// 成交方式
export const winBidRules = [
  {
    label: '最低价成交法',
    value: '1'
  },
  {
    label: '最高价成交法',
    value: '2'
  },
  {
    label: '平均价成交法(一)',
    value: '3'
  },
  {
    label: '次低价成交法',
    value: '4'
  },
  {
    label: '综合评分法',
    value: '5'
  },
  {
    label: '其他成交法',
    value: '6'
  },
  {
    label: '平均价成交法(二)',
    value: '7'
  },
  {
    label: '平均价成交法(三)',
    value: '8'
  },
  {
    label: '最低价成交法(履约分)',
    value: '9'
  }
]
