const PHONEREG = /^(0|86|17951)?(13[0-9]|19[0-9]|16[0-9]|15[0-9]|17[0-9]|18[0-9]|14[0-9])[0-9]{8}$/
const FIXPHONEREG = /^(0\d{2,3}-|\s)?\d{7,8}$/
const CONTACTPHONEREG = /^(0\d{2,4}-|\s)?\d{7,11}$/ // 联系方式：固话 + 手机 + 400
const EMAILREG = /^\w+([-+.´]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
const POSITIVE_INTEGER = /^[1-9]\d*$/ // 检验正整数
const FAXREG = /^(([0+]\d{2,3}-)?(0\d{2,3})-)(\d{7,8})(-(\d{3,}))?$/ // 传真号码校验

export {
  PHONEREG,
  FIXPHONEREG,
  CONTACTPHONEREG,
  EMAILREG,
  POSITIVE_INTEGER,
  FAXREG
}
