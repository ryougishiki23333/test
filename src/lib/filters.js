/* eslint-disable no-unreachable */
import Vue from 'vue'
import dayjs from 'dayjs'
import 'dayjs/locale/zh-cn'

Vue.filter('dayjs', function (dataStr, pattern = 'YYYY-MM-DD HH:mm:ss') {
  if (!dataStr) return ''
  return dayjs(dataStr).format(pattern)
})

Vue.prototype.$moduleSwitch = function (key) {
  switch (key) {
    case 'grade':
      return 'GProjectCreateOrUpdate'
      break;
  
    default:
      return false
      break;
  }
}