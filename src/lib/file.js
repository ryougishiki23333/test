import axios from 'axios'
import { fileService as FileService, middlewareService } from '@/lib/axios'
import { store } from '@ice/stark-data'

export const fileUtil = {
  uploadInit () {
    const action = 'https://middleware.choicelink.cn/uploadCenter/upload/simple'
    const headers = {
      "Abp.TenantId": store.get('bTenantId'),
      "Authorization": `Bearer ${store.get('bToken')}`
    }
    const extradata = {
      bucketName: 'upload-center',
      expireTime: '2023-08-16 10:48:19',
      isPubilc: true,
      path: 'choicelink/',
      fileName: '',
      remarks: ''
    }
    return {action, headers, extradata}
  },
  async downloadInit (userId, fileType, bucket, dir) {
    if (!userId || !fileType || !bucket) {
      console.error('缺失参数')
      return
    }
    const rep = await FileService.get('/api/services/app/AbpAliOss/GetSign', {
      params: {
        dir: dir,
        bucket: bucket
      }
    })
    const obj = rep.result
    const extradata = {
      'key': obj['dir'], // 未加后缀
      'policy': obj['policy'],
      'OSSAccessKeyId': obj['accessid'],
      'success_action_status': '200', // 让服务端返回200,不然，默认会返回204calculate_object_name(filename)
      'callback': obj['callback'],
      'signature': obj['signature'],
      'x:objectid': '',
      'x:userid': userId, // 用户id
      'x:filetype': fileType,
      'x:objectidtype': '',
      'x:displayname': '' // 文件名称
    }
    const action = obj['host']
    return {
      extradata, // 上传附带基本参数
      action // 上传地址
    }
  },
  // 随机后缀名
  random_string (len = 32) {
    const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ+abcdefghijklmnopqrstuvwxyz-012345678*'
    const maxPos = chars.length
    let pwd = ''
    for (var i = 0; i < len; i++) {
      pwd += chars.charAt(Math.floor(Math.random() * maxPos))
    }
    return pwd
  },
  // 获取后缀
  get_suffix (filename) {
    const pos = filename.lastIndexOf('.')
    let suffix = ''
    if (pos !== -1) {
      suffix = filename.substring(pos)
    }
    return suffix
  },
  async downLoad (fileId) {
    const rep = await middlewareService.get('/uploadCenter/getDownloadUrl', {
      params: {
        id: fileId
      }
    })
    return rep.result
  },
  async downLoadSampleFile (filename) {
    const rep = await FileService.get('/api/services/app/AbpAliOss/GetSampleDownloadUrl', {
      params: {
        bucket: 'choicelink-public',
        filename: filename
      }
    })
    return rep.result
  },
  async downLoadBlob (fileId) {
    const rep = await FileService.get('/api/services/app/AbpAliOss/GetDownUrl', {
      params: {
        id: fileId
      }
    })
    const fileRep = await axios.get(rep.result, { responseType: 'blob' })
    const blob = fileRep.data
    const fileUrl = window.URL.createObjectURL(blob)
    return fileUrl
  },
  // 获取文件记录
  getOssFileList (params) {
    return FileService.get('/api/services/app/AbpAliOss/GetListByObjectId', {
      params
    })
  },
  // 抽取word数据
  LoadHtmlTable (params) {
    return FileService.post('/api/services/app/WordHelpers/LoadHtmlTable', params)
  }
}
