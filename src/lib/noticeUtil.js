import moment from 'moment'
export default {
  getNoticeStatus: notice => {
    const currentTime = new Date(moment().format('YYYY-MM-DD HH:mm:ss')).getTime()
    const applyTimeBegin = new Date(notice.signUpStartTime).getTime()
    const applyTimeEnd = new Date(notice.signUpEndTime).getTime()
    const uppriceTimeBegin = new Date(notice.quotationStartTime).getTime()
    const uppriceTimeEnd = new Date(notice.quotationEndTime).getTime()
    const secUppriceTimeBegin = new Date(notice.secQuoteStartTime).getTime()
    const secUppriceTimeEnd = new Date(notice.secQuoteEndTime).getTime()
    if (notice.state === "0") {
      return 0
    } else if (notice.state === "1") {
      return 1
    } else if (notice.state === "2") {
      return 2
    } else if (notice.state === "3") {
      if (notice.secQuoteStartTime) { // 判断是否二次报价
        if (uppriceTimeEnd < currentTime && currentTime <= secUppriceTimeBegin) { // 即将二次报价
          return 33
        } else if (secUppriceTimeBegin < currentTime && currentTime <= secUppriceTimeEnd) { // 正在报价
          return 34
        } else if (currentTime > secUppriceTimeEnd) { // 已结束
          return 35
        } else { // 接口时间错误情况
          return 36
        }
      }
      if (currentTime < applyTimeBegin) { // 即将报名
        return 31
      } else if (applyTimeBegin < currentTime && currentTime <= applyTimeEnd) { // 正在报名
        return 32
      } else if (applyTimeEnd < currentTime && currentTime <= uppriceTimeBegin) { // 即将报价
        return 33
      } else if (uppriceTimeBegin < currentTime && currentTime <= uppriceTimeEnd) { // 正在报价
        return 34
      } else if (currentTime > uppriceTimeEnd) { // 35 报价结束
        return notice.auditState === '11' ? 37 : 35 // 37 结果审核中
        // return 35
      } else { // 接口时间错误情况
        return 36
      }
    } else if (notice.state === "4") {
      return 4
    } else if (notice.state === "5") {
      return 5
    } else if (notice.state === "6") {
      return 6
    } else if (notice.state === "7") {
      return 7
    }
  },
  /*
  * 分别获取开始时间，结束时间，是为了每秒钟更新一次进度,JS的interval是不精确的
  * 每一轮interval都重新获取一次当前时间 new date().getTime
  * 然后用interval，每秒给当前时间增加一秒
  * */
  getStartTime: notice => {
    const currentTime = new Date().getTime()
    const noticeDate = new Date(notice.creationTime).getTime()// 发布公告时间
    const applyTimeBegin = new Date(notice.signUpStartTime).getTime()
    const applyTimeEnd = new Date(notice.signUpEndTime).getTime()
    const uppriceTimeBegin = new Date(notice.quotationStartTime).getTime()
    const uppriceTimeEnd = new Date(notice.quotationEndTime).getTime()
    const secUppriceTimeBegin = new Date(notice.secQuoteStartTime).getTime()
    const secUppriceTimeEnd = new Date(notice.secQuoteEndTime).getTime()
    if (notice.secQuoteStartTime) { // 判断是否二次报价
      if (uppriceTimeEnd < currentTime && currentTime <= secUppriceTimeBegin) { // 即将二次报价
        return uppriceTimeEnd
      } else if (secUppriceTimeBegin < currentTime && currentTime <= secUppriceTimeEnd) { // 正在二次报价
        return secUppriceTimeBegin
      } else {
        return secUppriceTimeEnd
      }
    }
    if (currentTime < applyTimeBegin) { // 即将报名
      return noticeDate
    } else if (applyTimeBegin < currentTime && currentTime <= applyTimeEnd) { // 正在报名
      return applyTimeBegin
    } else if (applyTimeEnd < currentTime && currentTime <= uppriceTimeBegin) { // 即将报价
      return applyTimeEnd
    } else if (uppriceTimeBegin < currentTime && currentTime <= uppriceTimeEnd) { // 正在报价
      return uppriceTimeBegin
    } else {
      return uppriceTimeEnd
    }
  },
  getEndTime: notice => {
    const currentTime = new Date().getTime()
    const applyTimeBegin = new Date(notice.signUpStartTime).getTime()
    const applyTimeEnd = new Date(notice.signUpEndTime).getTime()
    const uppriceTimeBegin = new Date(notice.quotationStartTime).getTime()
    const uppriceTimeEnd = new Date(notice.quotationEndTime).getTime()
    const secUppriceTimeBegin = new Date(notice.secQuoteStartTime).getTime()
    const secUppriceTimeEnd = new Date(notice.secQuoteEndTime).getTime()
    if (notice.secQuoteStartTime) { // 判断是否二次报价
      if (uppriceTimeEnd < currentTime && currentTime <= secUppriceTimeBegin) { // 即将二次报价
        return secUppriceTimeBegin
      } else if (secUppriceTimeBegin < currentTime && currentTime <= secUppriceTimeEnd) { // 正在二次报价
        return secUppriceTimeEnd
      } else {
        return secUppriceTimeEnd
      }
    }
    if (currentTime < applyTimeBegin) { // 即将报名
      return applyTimeBegin
    } else if (applyTimeBegin < currentTime && currentTime <= applyTimeEnd) { // 正在报名
      return applyTimeEnd
    } else if (applyTimeEnd < currentTime && currentTime <= uppriceTimeBegin) { // 即将报价
      return uppriceTimeBegin
    } else if (uppriceTimeBegin < currentTime && currentTime <= uppriceTimeEnd) { // 正在报价
      return uppriceTimeEnd
    } else {
      return uppriceTimeEnd
    }
  }
}
