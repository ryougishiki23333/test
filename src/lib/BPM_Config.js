/**
 * 根据平台ID获取BPM流程Key
 * @param {Number} tenantId
 * @returns
 */
 export function getBPMProcessKey (tenantId) {
  if (!tenantId) throw new Error('无tenantId')
  const ProcessKey = {
    // [tenantId]: [processKey]
    2238: 'Process_06jb7pvQHFileTest',
    2191: 'Process_06jb7pvQHNotice',
    2142: 'Process_06jb7pvQHNotice', // 谦和
    2224: 'Process_06jb7pvFSSFYNotice' // 佛山市妇幼代理平台
  }
  return ProcessKey[tenantId]
}
