const { ifGlobalUrl } = require('./globalConfig')

const port = 6004

if (ifGlobalUrl) {
    const urlConfig = window.urlConfig
    __webpack_public_path__ = process.env.NODE_ENV === 'production' ? `https://qy.choicelink.cn:8354/wpQuoteChannel/public/` : `http://localhost:${port}/`
} else {
    __webpack_public_path__ = process.env.NODE_ENV === 'production' ? 'https://qy.choicelink.cn:8354/wpQuoteChannel/public/' : `http://localhost:${port}/`
}
