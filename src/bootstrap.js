import Vue from 'vue';
// import ElementUI from 'element-ui';
// import 'element-ui/lib/theme-chalk/index.css';
// import FormMaking from 'federationModule/formMakingJs';
// import 'federationModule/formMakingCss';
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.less';

import spin from './lib/spin/spin'

// Vue.use(ElementUI);
Vue.use(Antd);
// Vue.use(FormMaking);

//  全局spin
Vue.use(spin)

import App from './App.vue';
export const app = App
