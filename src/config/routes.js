import { renderNotFound } from '@ice/stark-app';
// import { BasicLayout, BlankLayout } from '@/layouts'
import List from '@/pages/List';
import Detail from '@/pages/Detail';
import Home from '@/pages/Home';

// const routerConfig = [
//   {
//     path: '/',
//     component: BasicLayout,
//     children: [
//       { path: '/', component: Home, exact: true },
//       { path: '/list', component: List },
//       { path: '/detail', component: Detail },
//       {
//         path: '*',
//         component: () => renderNotFound(),
//       },
//     ],
//   },
// ];

const routerConfig = [
          {
            path: '/',
            name: 'enroll',
            component: () => import('@/pages/QuoteChannel/toQuote/toQuote'),
            meta: { title: '报价通道', showHeadTabs: true }
          },
          {
            path: '/bid-notice/:projectId',
            name: 'SignUpProgressBidNotice',
            props: true,
            meta: { title: '项目详情', keepAlive: false, mode: 'timeline' },
            component: () => import('@/pages/QuoteChannel/noticeDetails/BidNotice')
          }
        ]
export default routerConfig;
