import './globalPath.js'
import Vue from 'vue';
import { isInIcestark, setLibraryName } from '@ice/stark-app';
import './styles/global.less';
import store from '@/store';
// import App from './App.vue';
import router from './router';
import './styles/formMaking.css'

import '@/lib/filters' // 全局引用 dayjs

import VueStorage from 'vue-ls'
import { lsOptions } from '@/lib/lsOptions'

Vue.use(VueStorage, lsOptions)

Vue.config.productionTip = false;

let vue = null;

// set in vue.config.js
setLibraryName('icestark-vue-demo');

export function mount(props) {
  import('./bootstrap.js').then((module) =>{
    const { container } = props;
    vue = new Vue({
      router,
      store,
      mounted: () => {
        console.log('App mounted');
      },
      destroyed: () => {
        console.log('App destroyed');
      },
      render: h => h(module.app),
    }).$mount();
    
    // for vue don't replace mountNode
    container.innerHTML = '';
    container.appendChild(vue.$el);
  })
}

export function unmount() {
  if (vue) vue.$destroy();
  vue.$el.innerHTML = '';
  vue = null;
}

if (!isInIcestark()) {
  // 初始化 vue 项目
  import('./bootstrap.js').then((module) => {
    // eslint-disable-next-line no-new
    new Vue({
      router,
      store,
      el: '#app',
      mounted: () => {
        console.log('App mounted');
      },
      destroyed: () => {
        console.log('App destroyed');
      },
      render: h => h(module.app),
    });
  })
}
