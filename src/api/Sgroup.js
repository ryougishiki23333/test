import { ProjectService as axios } from '@/lib/axios'

const api = {
  BatchCreateOrUpdateSgroup: '/api/services/app/Sgroup/BatchCreateOrUpdate', // 新增或修改包组
  DeleteSgroup: '/api/services/app/Sgroup/Delete', // 删除包组
  BatchDeleteSgroup: '/api/services/app/Sgroup/BatchDelete' // 批量删除包组
}

export default api

// 新增或修改包组
export function BatchCreateOrUpdateSgroup (parameter) {
  return axios({
    url: api.BatchCreateOrUpdateSgroup,
    method: 'post',
    data: parameter
  })
}

// 删除包组
export function DeleteSgroup (parameter) {
  return axios({
    url: api.DeleteSgroup,
    method: 'delete',
    params: parameter
  })
}

// 批量删除包组
export function BatchDeleteSgroup (parameter) {
  return axios({
    url: api.BatchDeleteSgroup,
    method: 'post',
    data: parameter
  })
}
