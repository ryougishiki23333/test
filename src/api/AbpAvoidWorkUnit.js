import { zjkService as zjkAxios } from '@/lib/axios'

const api = {
  BatchCreateOrUpdate: '/api/services/app/AbpAvoidWorkUnit/BatchCreateOrUpdate',
  Delete: '/api/services/app/AbpAvoidWorkUnit/Delete',
  GetPaged: '/api/services/app/AbpAvoidWorkUnit/GetPaged'
}

export function AbpAvoidWorkUnitBatchCreateOrUpdate (params) {
  return zjkAxios({
    url: api.BatchCreateOrUpdate,
    method: 'post',
    data: params
  })
}

export function AbpAvoidWorkUnitDelete (params) {
  return zjkAxios({
    url: api.Delete,
    method: 'delete',
    params
  })
}

export function AbpAvoidWorkUnitGetPaged (params) {
  return zjkAxios({
    url: api.GetPaged,
    method: 'get',
    params
  })
}
