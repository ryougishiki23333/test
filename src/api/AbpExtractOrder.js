// 专家抽取
// 专家表
import { zjkService as zjkAxios } from '@/lib/axios'

const api = {
  CreateOrUpdate: '/api/services/app/AbpExtractOrder/CreateOrUpdate',
  BatchCreateOrUpdateItemList: '/api/services/app/AbpExtractOrder/BatchCreateOrUpdateItemList', // 批量维护抽取品目
  DeleteExtractItem: '/api/services/app/AbpExtractOrder/DeleteExtractItem', // 删除抽取品目
  GetItemList: '/api/services/app/AbpExtractOrder/GetItemList', // 获取的品目列表信息
  GetPaged: '/api/services/app/AbpExtractOrder/GetPaged',
  GetByProjectId: '/api/services/app/AbpExtractOrder/GetByProjectId',
  CreateExtractResult: '/api/services/app/AbpExtractOrder/CreateExtractResult', // 创建抽取结果
  GetResultPaged: '/api/services/app/AbpExtractOrder/GetResultPaged', // 获取分页抽取结果
  GetResultList: '/api/services/app/AbpExtractOrder/GetResultList', // 获取抽取结果列表
  UpdateExtractResult: '/api/services/app/AbpExtractOrder/UpdateExtractResult', // 更新单条抽取结果
  BatchCreateOrUpdateResultList: '/api/services/app/AbpExtractOrder/BatchCreateOrUpdateResultList', // 批量更新抽取结果
  RemoveOrderResultById: '/api/services/app/AbpExtractOrder/RemoveOrderResultById' // 删除抽取结果
}

export function AbpExtractOrderCreateOrUpdate (params) {
  return zjkAxios({
    url: api.CreateOrUpdate,
    method: 'post',
    data: params
  })
}

export function ExtractOrderBatchCreateOrUpdateItemList (params) {
  return zjkAxios({
    url: api.BatchCreateOrUpdateItemList,
    method: 'post',
    data: params
  })
}

export function ExtractOrderDeleteExtractItem (params) {
  return zjkAxios({
    url: api.DeleteExtractItem,
    method: 'delete',
    params
  })
}

export function ExtractOrderGetItemList (params) {
  return zjkAxios({
    url: api.GetItemList,
    method: 'get',
    params
  })
}

export function AbpExtractOrderGetPaged (params) {
  return zjkAxios({
    url: api.GetPaged,
    method: 'get',
    params
  })
}

export function AbpExtractOrderGetResultList (params) {
  return zjkAxios({
    url: api.GetResultList,
    method: 'get',
    params
  })
}

export function AbpExtractOrderGetByProjectId (params) {
  return zjkAxios({
    url: api.GetByProjectId,
    method: 'get',
    params
  })
}

export function AbpExtractOrderCreateExtractResult (params) {
  return zjkAxios({
    url: api.CreateExtractResult,
    method: 'post',
    data: params
  })
}

export function AbpExtractOrderGetResultPaged (params) {
  return zjkAxios({
    url: api.GetResultPaged,
    method: 'get',
    params
  })
}

export function AbpExtractOrderUpdateExtractResult (params) {
  return zjkAxios({
    url: api.UpdateExtractResult,
    method: 'put',
    data: params
  })
}

export function AbpExtractOrderBatchCreateOrUpdateResultList (params) {
  return zjkAxios({
    url: api.BatchCreateOrUpdateResultList,
    method: 'post',
    data: params
  })
}

export function AbpExtractOrderRemoveOrderResultById (params) {
  return zjkAxios({
    url: api.RemoveOrderResultById,
    method: 'delete',
    params
  })
}
