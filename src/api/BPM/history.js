import { middlewareService as middlewareAxios } from '@/lib/axios'

const api = {
  getProcessProgress: '/camunda/history/getProcessProgress', // 获取流程进度
  getVariableHistory: '/camunda/history/getVariableHistory' // 获取待办任务列表
}

export function camundaHistoryGetProcessProgress (params) {
  return middlewareAxios({
    url: api.getProcessProgress,
    method: 'get',
    params
  })
}

export function camundaHistoryGetVariableHistory (params) {
  return middlewareAxios({
    url: api.getVariableHistory,
    method: 'get',
    params
  })
}

export default {
  camundaHistoryGetProcessProgress,
  camundaHistoryGetVariableHistory
}
