import { middlewareService as middlewareAxios } from '@/lib/axios'

const api = {
  addInstance: '/camunda/process/addInstance', // 加签
  circulate: '/camunda/process/circulate', // 流转流程
  getByCircledUserId: '/camunda/process/getByCircledUserId', // 获取流转过的流程实例列表
  getByStartUserId: '/camunda/process/getByStartUserId', // 获取发起的流程实例列表
  getCountByTaskKey: '/camunda/process/getCountByTaskKey', // 获取当前任务进度数量
  getInstanceByBusinessKeyLike: '/camunda/process/getInstanceByBusinessKeyLike', // 根据businessKey查询实例
  start: '/camunda/process/start' // 启动实例
}

export function camundaProcessAddInstance (params) {
  return middlewareAxios({
    url: api.addInstance,
    method: 'post',
    data: params
  })
}

export function camundaProcessCirculate (taskId, form) {
  return middlewareAxios({
    url: `${api.circulate}?taskId=${taskId}`,
    method: 'post',
    data: { form }
  })
}

export function camundaProcessGetByCircledUserId (params) {
  return middlewareAxios({
    url: api.getByCircledUserId,
    method: 'get',
    params
  })
}

export function camundaProcessGetByStartUserId (params) {
  return middlewareAxios({
    url: api.getByStartUserId,
    method: 'get',
    params
  })
}

export function camundaProcessGetCountByTaskKey (params) {
  return middlewareAxios({
    url: api.getCountByTaskKey,
    method: 'get',
    params
  })
}

export function camundaProcessGetInstanceByBusinessKeyLike (params) {
  return middlewareAxios({
    url: api.getInstanceByBusinessKeyLike,
    method: 'get',
    params
  })
}

export function camundaProcessStart (key, params) {
  return middlewareAxios({
    url: `${api.start}?key=${key}`,
    method: 'post',
    data: params
  })
}

export default {
  camundaProcessAddInstance,
  camundaProcessCirculate,
  camundaProcessGetByCircledUserId,
  camundaProcessGetByStartUserId,
  camundaProcessGetCountByTaskKey,
  camundaProcessGetInstanceByBusinessKeyLike,
  camundaProcessStart
}
