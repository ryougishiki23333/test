import { middlewareService as middlewareAxios } from '@/lib/axios'

const api = {
  autoClaim: '/camunda/task/autoClaim', // 任务自动认领
  claim: '/camunda/task/claim', // 任务认领
  getTasksByProcessInstanceId: '/camunda/task/getTasksByProcessInstanceId', // 根据流程实例id获取任务列表
  taskAssignee: '/camunda/task/taskAssignee', // 获取待办任务列表
  taskCandidateUser: '/camunda/task/taskCandidateUser', // 获取候选任务列表
  businessKey: '/camunda/task/taskCandidateUser/businessKey', // 获取候选任务列表(多businessKey)
  transfer: '/camunda/task/transfer' // 任务转让
}

export function camundaTaskAutoClaim (params) {
  return middlewareAxios({
    url: api.autoClaim,
    method: 'post',
    data: params
  })
}

export function camundaTaskClaim (taskId, userId) {
  return middlewareAxios({
    url: `${api.claim}?taskId=${taskId}&userId=${userId}`,
    method: 'post'
  })
}

export function camundaTaskGetTasksByProcessInstanceId (processInstanceId) {
  return middlewareAxios({
    url: api.getTasksByProcessInstanceId,
    method: 'get',
    params: { processInstanceId }
  })
}

export function camundaTaskTaskAssignee (params) {
  return middlewareAxios({
    url: api.taskAssignee,
    method: 'get',
    params
  })
}

export function camundaTaskTaskCandidateUser (params) {
  return middlewareAxios({
    url: api.taskCandidateUser,
    method: 'get',
    params
  })
}

export function camundaTaskBusinessKey (params) {
  return middlewareAxios({
    url: api.businessKey,
    method: 'get',
    params
  })
}

export function camundaTaskTransfer (params) {
  return middlewareAxios({
    url: api.transfer,
    method: 'post',
    data: params
  })
}

export default {
  camundaTaskAutoClaim,
  camundaTaskClaim,
  camundaTaskGetTasksByProcessInstanceId,
  camundaTaskTaskAssignee,
  camundaTaskTaskCandidateUser,
  camundaTaskBusinessKey,
  camundaTaskTransfer
}
