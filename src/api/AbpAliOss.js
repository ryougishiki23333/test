// 文件上传
import { fileService as fileAxios } from '@/lib/axios'
import store from '@/store'
const api = {
  del: '/api/services/app/AbpAliOss/Delete', // 删除AbpAliOss信息的方法
  GetDownUrl: '/api/services/app/AbpAliOss/GetDownUrl' // 获取下载连接
}

export function del (params) {
  return fileAxios({
    url: api.del,
    method: 'delete',
    headers: {
      Authorization: 'Bearer ' + store.state.user.token
    },
    params
  })
}

export function GetDownUrl (params) {
  return fileAxios({
    url: api.GetDownUrl,
    method: 'get',
    params
  })
}
