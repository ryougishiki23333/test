// 中间件
import { userCenterService } from '@/lib/axios'

const api = {
  getDictList: '/api/services/app/AbpDataDict/GetList', // 获取地区
  getManagersList: '/api/services/app/DistrictManagers/GetList', // 获取地区经理
}

export function dtGetList (filter) {
  return userCenterService({
    url: api.getDictList,
    method: 'get',
    params: {
      filter
    }
  })
}
export function managerGetList (filter) {
  return userCenterService({
    url: api.getManagersList,
    method: 'get',
    params: {
      filter
    }
  })
}