import { MessageService as MessageAxios } from '@/lib/axios'


const api = {
  SendDXWPlatformSmsAsync: '/api/services/app/DXWSms/SendDXWPlatformSmsAsync', // 短信网配置发送短信
  SendFlow34Message: '/api/services/app/AutoMessage/SendFlow34Message' // 发送专家邀请短信
}

// 短信网配置发送短信
export function SendDXWPlatformSmsAsync (param) {
  return MessageAxios({
    url: api.SendDXWPlatformSmsAsync,
    method: 'post',
    data: param
  })
}

// 发送专家邀请短信
export function SendFlow34Message (param) {
  return MessageAxios({
    url: api.SendFlow34Message,
    method: 'post',
    data: param
  })
}
