import { zjkService as zjkAxios } from '@/lib/axios'

const api = {
  BatchCreateOrUpdate: '/api/services/app/AbpAvoidPerson/BatchCreateOrUpdate',
  Delete: '/api/services/app/AbpAvoidPerson/Delete',
  GetPaged: '/api/services/app/AbpAvoidPerson/GetPaged'
}

export function AbpAvoidPersonBatchCreateOrUpdate (params) {
  return zjkAxios({
    url: api.BatchCreateOrUpdate,
    method: 'post',
    data: params
  })
}

export function AbpAvoidPersonDelete (params) {
  return zjkAxios({
    url: api.Delete,
    method: 'delete',
    params
  })
}

export function AbpAvoidPersonGetPaged (params) {
  return zjkAxios({
    url: api.GetPaged,
    method: 'get',
    params
  })
}
