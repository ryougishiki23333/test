// 专家表-专家管理
import { zjkService as zjkAxios } from '@/lib/axios'

const api = {
  CreateOrUpdate: '/api/services/app/AbpNewExpert/CreateOrUpdate',
  GetPaged: '/api/services/app/AbpNewExpert/GetPaged',
  Delete: '/api/services/app/AbpNewExpert/Delete'
}

export function AbpNewExpertCreateOrUpdate (params) {
  return zjkAxios({
    url: api.CreateOrUpdate,
    method: 'post',
    data: params
  })
}

export function AbpNewExpertGetPaged (params) {
  return zjkAxios({
    url: api.GetPaged,
    method: 'get',
    params
  })
}

export function AbpNewExpertDelete (params) {
  return zjkAxios({
    url: api.Delete,
    method: 'delete',
    params
  })
}
