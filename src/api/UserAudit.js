// 5002
import { PlatformService as userAxios, userCenterService, middlewareAxios } from '@/lib/axios'

const api = {
  GetPaged: '/api/services/app/UserAudit/GetPaged',
  GetByUserId: '/api/services/app/UserAudit/GetByUserId',
  UpdateStatus: '/api/services/app/UserAudit/UpdateStatus',
  Delete: '/api/services/app/UserAudit/Delete',
  DeleteByUserId: '/api/services/app/UserAudit/DeleteByUserId',
  RegisterAnyUserAsync: '/api/TokenAuth/RegisterAnyUserAsync', // 免审授权
  getUserList: '/api/services/app/User/GetUserCompanyList' // 获取注册待审核数据
}

export function getUserList (params) {
  return userCenterService({
    url: api.getUserList,
    method: 'get',
    params
  })
}

export function RegisterAnyUserAsync (data) {
  return userAxios({
    url: api.RegisterAnyUserAsync,
    method: 'post',
    data
  })
}

export function UserAuditGetPaged (params) {
  return userAxios({
    url: api.GetPaged,
    method: 'get',
    params
  })
}

export function UserAuditGetByUserId (params) {
  return userAxios({
    url: api.GetByUserId,
    method: 'get',
    params
  })
}

export function UserAuditUpdateStatus (params) {
  return userAxios({
    url: api.UpdateStatus,
    method: 'put',
    data: params
  })
}

export function UserAuditDelete (params) {
  return userAxios({
    url: api.Delete,
    method: 'delete',
    params
  })
}

export function UserAuditDeleteByUserId (params) {
  return userAxios({
    url: api.DeleteByUserId,
    method: 'delete',
    params
  })
}
