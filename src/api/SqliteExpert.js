// 中间件
import { SqliteService as middlewareAxios } from '@/lib/axios'

const api = {
    // 专家
    SqliteExpertFilterPaged: '/productData/expert/filter/getPaged',
    SqliteExpertInsert: '/productData/expert/insert',
    SqliteExpertUpdate: '/productData/expert/update',
    SqliteExpertDelete: '/productData/expert/delete',

    // 专家附件
    // SqliteExpertExtraInsert: '/productData/expertExtra/insert',
    SqliteExpertExtraUpdate: '/productData/expertExtra/update',
    // SqliteExpertExtraDelete: '/productData/expertExtra/delete',
}

export function SqliteExpertFilterPaged (params) {
    return middlewareAxios({
        url: api.SqliteExpertFilterPaged,
        method: 'get',
        params
    })
}

export function SqliteExpertInsert (params) {
    return middlewareAxios({
        url: api.SqliteExpertInsert,
        method: 'post',
        data: params
    })
}

export function SqliteExpertUpdate (params) {
    return middlewareAxios({
        url: api.SqliteExpertUpdate,
        method: 'put',
        data: params
    })
}

export function SqliteExpertDelete (params) {
    return middlewareAxios({
        url: api.SqliteExpertDelete,
        method: 'delete',
        params
    })
}

export function SqliteExpertExtraUpdate (params) {
    return middlewareAxios({
        url: api.SqliteExpertExtraUpdate,
        method: 'put',
        data: params
    })
}
