// 获取服务器信息
import { ProjectService as axios } from '@/lib/axios'

const api = {
  GetCurrentServerDate: '/api/services/app/Session/GetCurrentServerDate' // 获取服务时间
}

export function GetCurrentServerDate () {
  return axios({
    url: api.GetCurrentServerDate,
    method: 'get'
  })
}
