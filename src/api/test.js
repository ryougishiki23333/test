import { ProjectService as axios } from '@/lib/axios'
const name = 'test'

const api = {
  get: ['1'],
  post: ['2'],
  delete: ['3']
}
const res = {}
for (const key in api) {
  const element = api[key]
  element.forEach(v => {
    res[`${name}${v}`] = axios({
      url: v,
      method: key
      // params: params
    })
  })
}

export default res
