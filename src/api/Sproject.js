import { ProjectService as axios } from '@/lib/axios'

const api = {
  GetProjectNum: '/api/services/app/Sproject/GetProjectNum', // 生成项目编号
  GetPaged: '/api/services/app/Sproject/GetPaged', // 项目分页查询
  GetById: '/api/services/app/Sproject/GetById', // 通过指定id获取项目信息
  GetByIdAll: '/api/services/app/Sproject/GetByIdAll', // GetByIdAll
  GetSprojectDetail: '/api/services/app/Sproject/GetSprojectDetail', // 获取项目报名详情

  CreateOrUpdate: '/api/services/app/Sproject/CreateOrUpdate', // 添加或者修改项目的公共方法
  CreateOrUpdateInfo: '/api/services/app/Sproject/CreateOrUpdateInfo', // 创建更新项目和其他数据

  GetForEdit: '/api/services/app/Sproject/GetForEdit', // 获取编辑 项目
  CirculationProjectFlowById: '/api/services/app/Sproject/CirculationProjectFlowById', // 根据项目ID及行为流转项目
  GetFlowRecordListByProjectId: '/api/services/app/Sproject/GetFlowRecordListByProjectId' // 获取项目流转记录

}

// 生成项目编号
export function GetProjectNum () {
  return axios({
    url: api.GetProjectNum,
    method: 'get'
  })
}

export function SprojectGetPaged (parameter) {
  const params = {
    ...parameter
  }
  params.Filter = !params.Filter ? 'mode=3' : params.Filter + ' and mode=3'
  return axios({
    url: api.GetPaged,
    method: 'get',
    params
  })
}

export function GetSprojectById (params) {
  return axios({
    url: api.GetById,
    method: 'get',
    params
  })
}

export function GetByIdAll (params) {
  return axios({
    url: api.GetByIdAll,
    method: 'get',
    params
  })
}

export function GetSprojectDetail (parameter) {
  const params = {
    ...parameter
  }
  params.Filter = !params.Filter ? 'mode=3' : params.Filter + ' and mode=3'
  return axios({
    url: api.GetSprojectDetail,
    method: 'get',
    params
  })
}

export function CreateOrUpdate (params) {
  params.sproject.mode = 3
  return axios({
    url: api.CreateOrUpdate,
    method: 'post',
    data: params
  })
}

export function CreateOrUpdateInfo (params) {
  params.mode = 3
  return axios({
    url: api.CreateOrUpdateInfo,
    method: 'post',
    data: params
  })
}

export function BatchDelete (params) {
  return axios({
    url: api.BatchDelete,
    method: 'post',
    data: params
  })
}

// 根据项目ID及行为流转项目
export function CirculationProjectFlowById (params) {
  params.mode = 3
  return axios({
    url: api.CirculationProjectFlowById,
    method: 'post',
    data: params
  })
}
// 根据项目ID获取流转记录
export function GetFlowRecordListByProjectId (params) {
  return axios({
    url: api.GetFlowRecordListByProjectId,
    method: 'get',
    params
  })
}
