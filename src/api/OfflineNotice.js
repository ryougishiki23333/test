// 公告
import { ProjectService as axios } from '@/lib/axios'
import store from '@/store/index'

const api = {
  GetPaged: '/api/services/app/OfflineNotice/GetPaged',
  CreateOrUpdateInfo: '/api/services/app/OfflineNotice/CreateOrUpdateInfo' // 添加线下公告（带附件）
}

export function noticeGetPaged (params) {
  return axios({
    url: api.GetPaged,
    method: 'get',
    params
  })
}

export function noticeCreateOrUpdate (parameter, project) {
  console.log('parameter', parameter)
  let params = {}
  if (project) {
    params = Object.assign({
      signUpStartTime: project.signUpStartTime,
      signUpEndTime: project.signUpEndTime,
      quotationStartTime: project.quotationStartTime,
      quotationEndTime: project.quotationEndTime,
      projectBudget: project.projectBudget,
      projectName: project.projectName,
      platformName: project.agencyUnit,
      purchaseName: project.purchaserUnit,
      publicId: store.state.user.info.tenant.tenancyName
    }, parameter)
  } else {
    params = { ...parameter }
  }
  return axios({
    url: api.CreateOrUpdateInfo,
    method: 'post',
    data: params
  })
}
