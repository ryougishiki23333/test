// 5001
import { userCenterService as userCenterAxios, emptyService as emptyAxios } from '@/lib/axios'

const api = {
  AbpCompany: '/api/services/app/AbpCompany/GetById',
  HonorFileGetPaged: '/api/services/app/HonorFile/GetPaged',
  PostSimpleUser: '/api/services/app/User/PostSimpleUser', // 获取简易用户信息
  GetSimpleUser: '/api/services/app/User/GetSimpleUser',
  RegisterExpert: 'https://api.choicelink.cn/UserCenter/api/services/app/Account/RegisterExpert', // 专家注册
  ValidAccount: '/api/services/app/Account/ValidAccount', // 验证账号
  AbpExpertUserCreateOrUpdate: '/api/services/app/AbpExpertUser/CreateOrUpdate', // 添加或者修改 专家用户扩展表 的公共方法
  AbpExpertFileBatchCreateOrUpdate: '/api/services/app/AbpExpertFile/BatchCreateOrUpdate' // 添加或者修改 专家资质文件信息 的公共方法
}

export function AbpCompanyGetById (params) {
  return userCenterAxios({
    url: api.AbpCompany,
    method: 'get',
    params
  })
}

export function HonorFileGetPaged (params) {
  return userCenterAxios({
    url: api.HonorFileGetPaged,
    method: 'get',
    params
  })
}

// 获取简易用户信息 多个
export function PostSimpleUser (params) {
  return userCenterAxios({
    url: api.PostSimpleUser,
    method: 'post',
    data: params
  })
}

// 获取简易用户信息
export function GetSimpleUser (params) {
  return userCenterAxios({
    url: api.GetSimpleUser,
    method: 'get',
    params
  })
}

// A平台专家注册
export function RegisterExpert (params) {
  return emptyAxios({
    url: api.RegisterExpert,
    method: 'post',
    data: params
  })
}

// 验证账号
export function ValidAccount (params) {
  return userCenterAxios({
    url: api.ValidAccount,
    method: 'post',
    params
  })
}

// 添加或者修改 专家用户扩展表 的公共方法
// 注意带上ID则为update,不带则为create
export function AbpExpertUserCreateOrUpdate (params) {
  return userCenterAxios({
    url: api.AbpExpertUserCreateOrUpdate,
    method: 'post',
    data: params
  })
}

// 批量添加或者修改 专家资质文件信息 的公共方法
// 注意带上ID则为update,不带则为create
export function AbpExpertFileBatchCreateOrUpdate (params) {
  return userCenterAxios({
    url: api.AbpExpertFileBatchCreateOrUpdate,
    method: 'post',
    data: params
  })
}
