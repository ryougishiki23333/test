// 中间件
import { SqliteService as middlewareAxios, MiddleService, ProjectService } from '@/lib/axios'

const api = {
  // 动态模板
  SqliteTemplateInsert: '/productData/dynamicTemplate/insert', // 新增动态模板
  SqliteTemplateGetPaged: '/productData/dynamicTemplate/getPaged', // 获取动态模板分页信息

  SqliteGet: '/productData/api/v2/get', // 获取
  SqliteFind: '/productData/api/v2/find', // 汇总
  SqliteInsert: '/productData/api/v2/insert', // 新增
  SqliteUpdate: '/productData/api/v2/update', // 修改
  SqliteRefresh: '/productData/api/v2/reflash', // 刷新
  SqliteDelete: '/productData/api/v2/delete', // 删除

  // 代办列表
  SqliteToDoListGet: '/medical/sqlite/productData/toDoList/getPaged', // 获取代办列表

  // 项目
  SqliteProjectDelete: '/project/delete', // 删除项目信息
  SqliteProjectGetPaged: '/productData/project/getPaged', // 获取项目分页信息
  SqliteProjectFilterGetPaged: '/productData/project/filter/getPaged',
  SqliteProjectInsert: '/productData/project/insert', // 新增项目信息
  SqliteProjectUpdate: '/productData/project/update', // 修改项目信息
  SqliteProjectUpdateWithModel: '/productData/project/updateWithModel', // 修改项目信息(连带sproject模块)

  // 获取数据字典
  SqliteProductDataGetItemType: '/productData/dictionary/getByItemType',

  // 获取供应商
  SqliteprojectTransactionGet: '/productData/projectTransactionData/getPaged',

  // 获取项目的额外信息
  SqliteProductExtraGet: '/productData/projectExtra/getPaged',

  // 获取系统品目数据
  ProjectItemGet: '/api/services/app/ProjectItems/GetList',

  // 获取客户档案
  SqliteClientProfileGet:'/productData/clientProfile/getPaged',

  // 获取供应商报名
  SupplierFilterGet: '/productData/supplierSignUp/filter/getPaged',
  SupplierAndProjectFilterGet: '/productData/supplierSignUp/filter/getProjectPaged',
  SupplierGet: '/productData/supplierSignUp/getPaged',
  SupplierInsert: '/productData/supplierSignUp/insert',
  SupplierUpdate: '/productData/supplierSignUp/update',

  // 获取我（供应商）的项目
  fusionSupplierGet: '/productData/fusionSupplierSignUp/filter/getPaged'
}


//  java 修改a用户信息
export function aUserUpdateJava (data) {
  return MiddleService({
    url: '/ordinary/onlineProject/a/user/updateMessage',
    method: 'put',
    data
  })
}

export function SqliteTemplateInsert (params) {
  return middlewareAxios({
    url: api.SqliteTemplateInsert,
    method: 'post',
    data: params
  })
}

export function SqliteTemplateGetPaged (params) {
  return middlewareAxios({
    url: api.SqliteTemplateGetPaged,
    method: 'get',
    params
  })
}

export function SqliteGet (params) {
  return middlewareAxios({
    url: api.SqliteGet,
    method: 'get',
    params
  })
}

export function SqliteFind (params) {
  return middlewareAxios({
    url: api.SqliteFind,
    method: 'get',
    params
  })
}

export function SqliteInsert (params) {
  return middlewareAxios({
    url: api.SqliteInsert,
    method: 'post',
    data: params
  })
}

export function SqliteUpdate (params) {
  return middlewareAxios({
    url: api.SqliteUpdate,
    method: 'put',
    data: params
  })
}

export function SqliteRefresh (params) {
  return middlewareAxios({
    url: api.SqliteRefresh,
    method: 'get',
    params
  })
}

export function SqliteDelete (params) {
  return middlewareAxios({
    url: api.SqliteDelete,
    method: 'delete',
    params
  })
}

// 删除项目信息
export function SqliteProjectDelete (params) {
  return middlewareAxios({
    url: api.SqliteProjectDelete,
    method: 'delete',
    params
  })
}

// 获取项目分页信息
export function SqliteProjectGetPaged (params) {
  return middlewareAxios({
    url: api.SqliteProjectGetPaged,
    method: 'get',
    params
  })
}

// 获取项目过滤分页信息
export function SqliteProjectFilterGetPaged (params) {
  console.log(middlewareAxios)
  return middlewareAxios({
    url: api.SqliteProjectFilterGetPaged,
    method: 'get',
    params
  })
}

// 新增项目信息
export function SqliteProjectInsert (params) {
  return middlewareAxios({
    url: api.SqliteProjectInsert,
    method: 'post',
    data: params
  })
}

// 修改项目信息
export function SqliteProjectUpdate (params) {
  return middlewareAxios({
    url: api.SqliteProjectUpdate,
    method: 'put',
    data: params
  })
}

// 修改项目信息(连带sproject模块)
export function SqliteProjectUpdateWithModel (params) {
  return middlewareAxios({
    url: api.SqliteProjectUpdateWithModel,
    method: 'put',
    data: params
  })
}

// 通过sqlite获取数据字典单个数据
export function SqliteProductDataGetItemType (params) {
  return middlewareAxios({
    url: api.SqliteProductDataGetItemType,
    method: 'get',
    params
  })
}

// 获取代办列表
export function SqliteToDoListGet (params) {
  return MiddleService({
    url: api.SqliteToDoListGet,
    method: 'get',
    params
  })
}

// 获取供应商
export function SqliteprojectTransactionGet (params) {
  return middlewareAxios({
    url: api.SqliteprojectTransactionGet,
    method: 'get',
    params
  })
}

// 获取项目的额外信息
export function SqliteProductExtraGet (params) {
  return middlewareAxios({
    url: api.SqliteProductExtraGet,
    method: 'get',
    params
  })
}

// 获取供应商
export function ProjectItemGet (params) {
  return ProjectService({
    url: api.ProjectItemGet,
    method: 'get',
    params
  })
}

// 获取客户档案
export function SqliteClientProfileGet (params) {
  return middlewareAxios({
    url: api.SqliteClientProfileGet,
    method: 'get',
    params
  })
}

// 获取供应商报名filter
export function SupplierFilterGet (params) {
  return middlewareAxios({
    url: api.SupplierFilterGet,
    method: 'get',
    params
  })
}
// 获取项目和供应商报名filter
export function SupplierAndProjectFilterGet (params) {
  return middlewareAxios({
    url: api.SupplierAndProjectFilterGet,
    method: 'get',
    params
  })
}
// 获取供应商报名
export function SupplierGet (params) {
  return middlewareAxios({
    url: api.SupplierGet,
    method: 'get',
    params
  })
}
// 新增供应商报名
export function SupplierInsert (params) {
  return middlewareAxios({
    url: api.SupplierInsert,
    method: 'post',
    data: params
  })
}
// 删除供应商报名
export function SupplierDelete (params) {
  return middlewareAxios({
    url: api.SupplierDelete,
    method: 'put',
    data: params
  })
}

// 获取我（供应商）的项目
export function fusionSupplierGet (params) {
  return middlewareAxios({
    url: api.fusionSupplierGet,
    method: 'get',
    params
  })
}