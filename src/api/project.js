// 获取服务器信息
import { ProjectService as axios } from '@/lib/axios'
import store from '@/store/index'

const api = {
  create: '/api/services/app/Sproject/CreateOrUpdateInfo',
  createNum: '/api/services/app/Sproject/GetProjectNum',
  createOrUpdate: '/api/services/app/Sproject/CreateOrUpdate',
  getDetail: '/api/services/app/Sproject/GetSprojectDetail',
  getPage: '/api/services/app/Sproject/GetPaged',
  GetGysNum: '/api/services/app/MenuNum/GetGysNum',
  getBank: '/api/services/app/AbpBankAccount/GetPaged', // 获取开户行信息
  getProjectItems: '/api/services/app/ProjectItems/GetList', // 获取品目列表
  GetCurrentServerDate: '/api/services/app/Session/GetCurrentServerDate', // 获取服务器时间
  createExpressage: '/api/services/app/Expressage/CreateOrUpdate', // 创建物流管理单
  getExpressage: '/api/services/app/Expressage/GetPaged', // 查询物流管理单
  GetById: '/api/services/app/Sproject/GetById'
}

export function createProject (parameter) {
  return axios({
    url: api.create,
    method: 'post',
    data: parameter
  })
}

export function updateProject (parameter) {
  return axios({
    url: api.createOrUpdate,
    method: 'post',
    data: parameter
  })
}

export function createPorjectNum () {
  return axios({
    url: api.createNum,
    method: 'get'
  })
}

export function getProjectDetail (parameter) {
  return axios({
    url: api.getDetail,
    method: 'get',
    params: parameter
  })
}

export function getProjectNum (filter) {
  // 用户过滤
  const roles = store.state.user.roles
  const user = store.state.user.info.user
  // 角色项目过滤白名单
  const whiteList = ['Admin', '平台管理员', '审核人']
  const inWhiteList = roles.find(item => whiteList.includes(item))
  if (filter === '' && !inWhiteList) {
    filter = `creatorUserId == "${user.id}"`
  }
  if (filter !== '' && !inWhiteList) {
    filter += ` and creatorUserId == "${user.id}"`
  }
  return axios({
    url: api.getPage,
    method: 'get',
    params: {
      Filter: filter,
      MaxResultCount: 1,
      SkipCount: 0
    }
  })
}

export function GetGysNum (parameter) {
  return axios({
    url: api.GetGysNum,
    method: 'get',
    params: parameter
  })
}

export function getBank (parameter) {
  return axios({
    url: api.getBank,
    method: 'get',
    params: parameter
  })
}

export function getProjectItems (parameter) {
  return axios({
    url: api.getProjectItems,
    method: 'get',
    params: parameter
  })
}

export function GetCurrentServerDate () {
  return axios({
    url: api.GetCurrentServerDate,
    method: 'get'
  })
}

export function createExpressage (parameter) {
  return axios({
    url: api.createExpressage,
    method: 'post',
    data: parameter
  })
}

export function getExpressage (parameter) {
  return axios({
    url: api.getExpressage,
    method: 'get',
    params: parameter
  })
}

export function getProjectById (parameter) {
  return axios({
    url: api.GetById,
    method: 'get',
    params: parameter
  })
}
