// 获取投标人信息
import { PlatformService as userAxios } from '@/lib/axios'

const api = {
  GetPaged: '/api/services/app/UserCenterCompanyView/GetPaged',
  UserAuditGetPaged: '/api/services/app/UserAudit/GetPaged',
  UserByCompanyIds: '/api/services/app/User/UserByCompanyIds', // 根据公司ID集合查询
  CreateOrUpdate: '/api/services/app/TenantCompany/CreateOrUpdate',
  CompanyGetPaged: '/api/services/app/TenantCompany/GetPaged',
  UpdateStatus: '/api/services/app/UserAudit/UpdateStatus', // 更新平台用户审核状态(Status 默认0待审核，1未通过，2通过)
  UserGet: '/api/services/app/User/Get',
  UserUpdate: '/api/services/app/User/Update',
  TenantCompanyOutgoingByCompanyId: '/api/services/app/TenantCompany/TenantCompanyOutgoingByCompanyId', // 投标人出库
  UserListGet: '/api/services/app/User/GetUserListByRoleName', // 获取委托机构
}

export function supplierGetPaged (params) {
  return userAxios({
    url: api.GetPaged,
    method: 'get',
    params
  })
}

export function UserAuditGetPaged (params) {
  return userAxios({
    url: api.UserAuditGetPaged,
    method: 'get',
    params
  })
}

export function UserByCompanyIds (params) {
  return userAxios({
    url: api.UserByCompanyIds,
    method: 'post',
    data: params
  })
}

export function CompanyCreateOrUpdate (params) {
  return userAxios({
    url: api.CreateOrUpdate,
    method: 'post',
    data: params
  })
}

export function CompanyGetPaged (params) {
  return userAxios({
    url: api.CompanyGetPaged,
    method: 'get',
    params
  })
}

export function UpdateStatus (params) {
  return userAxios({
    url: api.UpdateStatus,
    method: 'put',
    data: params
  })
}

export function UserGet (params) {
  return userAxios({
    url: api.UserGet,
    method: 'get',
    params
  })
}

export function UserUpdate (params) {
  return userAxios({
    url: api.UserUpdate,
    method: 'put',
    data: params
  })
}

export function TenantCompanyOutgoingByCompanyId (params) {
  return userAxios({
    url: api.TenantCompanyOutgoingByCompanyId,
    method: 'post',
    data: params
  })
}

export function UserListGet (params) {
  return userAxios({
    url: api.UserListGet,
    method: 'get',
    params
  })
}