import { ProjectService as axios } from '@/lib/axios'

const api = {
  processSetting: '/api/services/app/EProjectFlowInstance/GetList',
  EProjectFlowNodeGetPaged: '/api/services/app/EProjectFlowNode/GetPaged' // 获取的分页列表信息
}

export default api

// 流程模板
export function getProcessSettingSelect () {
  return axios({
    url: api.processSetting,
    method: 'get'
  })
}

export function EProjectFlowNodeGetPaged (params) {
  return axios({
    url: api.EProjectFlowNodeGetPaged,
    method: 'get',
    params
  })
}
