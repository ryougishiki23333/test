// 5001
import { userCenterService as userCenterAxios } from '@/lib/axios'

const api = {
  GetByUserId: '/api/services/app/AbpExpertFile/GetByUserId' // 根据专家userid获取资质文件
}

export function AbpExpertFileGetByUserId (params) {
  return userCenterAxios({
    url: api.GetByUserId,
    method: 'get',
    params
  })
}
