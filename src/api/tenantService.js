// 服务费获取
import { ProjectService as axios } from '@/lib/axios'

const api = {
  getServiceChargeList: '/api/services/app/Sgroup/ServiceChargeList'
}

export function getServiceChargeList (paramter) {
  return axios({
    url: api.getServiceChargeList,
    method: 'post',
    data: paramter
  })
}
