import { ProjectService as axios } from '@/lib/axios'

const api = {
  user: '/api/services/app/User/GetPaged',
  role: '/role',
  service: '/serviceList',
  permission: '/permission',
  permissionNoPager: '/permission/no-pager',
  orgTree: '/org/tree',
  QRCode: '/api/TokenAuth/QRCode', // 获取小程序二维码
  getServiceRule: '/api/services/app/TenantService/GetTenantService'
}

export default api

export function getUserList (parameter, filterParam) {
  const { pageNo, pageSize } = parameter
  const { filter, sorting } = filterParam
  return axios({
    url: api.user,
    method: 'get',
    params: {
      filter: filter,
      Sorting: sorting,
      MaxResultCount: pageSize,
      SkipCount: (pageNo - 1) * pageSize
    }
  })
}

export function getRoleList (parameter) {
  return axios({
    url: api.role,
    method: 'get',
    params: parameter
  })
}

export function getServiceList (parameter) {
  return axios({
    url: api.service,
    method: 'get',
    params: parameter
  })
}

export function getPermissions (parameter) {
  return axios({
    url: api.permissionNoPager,
    method: 'get',
    params: parameter
  })
}

export function getOrgTree (parameter) {
  return axios({
    url: api.orgTree,
    method: 'get',
    params: parameter
  })
}

// id == 0 add     post
// id != 0 update  put
export function saveService (parameter) {
  return axios({
    url: api.service,
    method: parameter.id === 0 ? 'post' : 'put',
    data: parameter
  })
}

export function QRCode (param) {
  return axios({
    url: api.QRCode,
    method: 'post',
    data: param
  })
}

export function getServiceRule (paramter) {
  return axios({
    url: api.getServiceRule,
    method: 'get',
    params: paramter
  })
}
