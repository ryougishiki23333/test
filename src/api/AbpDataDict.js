import { userCenterService as userCenterAxios } from '@/lib/axios'

const api = {
  GetListByType: '/api/services/app/AbpDataDict/GetListByType', // 根据类型获取指定数据字典缓存列表
  AbpDataDictGetList: '/api/services/app/AbpDataDict/GetList' // 数据字典list
}

export function AbpDataDictGetListByType (params) {
  return userCenterAxios({
    url: api.GetListByType,
    method: 'get',
    params
  })
}

export function AbpDataDictGetList (params) {
  return userCenterAxios({
    url: api.AbpDataDictGetList,
    method: 'get',
    params
  })
}
