// 品目管理
import { zjkService as zjkAxios } from '@/lib/axios'

const api = {
  CreateOrUpdate: '/api/services/app/AbpItems/CreateOrUpdate',
  GetPaged: '/api/services/app/AbpItems/GetPaged',
  GetNextItemList: '/api/services/app/AbpItems/GetNextItemList', // 获取下级品目
  GetList: '/api/services/app/AbpItems/GetList' // 获取所有AbpItem
}

export function AbpItemsCreateOrUpdate (params) {
  return zjkAxios({
    url: api.CreateOrUpdate,
    method: 'post',
    data: params
  })
}

export function AbpItemsGetPaged (params) {
  return zjkAxios({
    url: api.GetPaged,
    method: 'get',
    params
  })
}

export function GetNextItemList (params) {
  return zjkAxios({
    url: api.GetNextItemList,
    method: 'get',
    params
  })
}

export function AbpItemsGetList (params) {
  return zjkAxios({
    url: api.GetList,
    method: 'get',
    params
  })
}
