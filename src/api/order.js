// 中间件
import { ProjectService as projectService } from '@/lib/axios'

const api = {
  getPaged: '/api/services/app/Orders/GetPaged',
  createOrder: '/api/services/app/Orders/CreateOrUpdate',
  batchCreateOrder: '/api/services/app/Orders/CreateOrUpdateList',
  // 催收次数添加
  AddDunningCount: '/api/services/app/Orders/AddDunningCount',
  // 对公支付审核记录查询
  GetPublishCheck: '/api/services/app/Orders/GetPublishCheck',
  // 对公支付审核记录创建
  CreatePublishCheck: '/api/services/app/Orders/CreatePublishCheck',
  // 订单project数据获取
  GetCreateOrderProjectById: '/api/services/app/Orders/GetCreateOrderProjectById',
  // 查询财务中心order
  GetPublishOrder: '/api/services/app/Orders/GetPublishOrder',
  // 创建财务中心订单
  CreatePublishOrder: '/api/services/app/Orders/CreatePublishOrder',
  // 更新财务中心Charges
  ModifyPublishChargesOrder: '/api/services/app/Orders/ModifyPublishChargesOrder',
  // 更新财务中心order
  UpdatePublishOrder: '/api/services/app/Orders/UpdatePublishOrder',
  // 删除订单中心订单
  BatchDeleteProjectOrder: '/api/services/app/Orders/BatchDelete',
  // 财务中心订单删除
  RemovePublishOrderByNo: '/api/services/app/Orders/RemovePublishOrderByNo'
}

export function createOrder (paramter) {
  return projectService({
    url: api.createOrder,
    method: 'post',
    data: paramter
  })
}

export function batchCreateOrder (paramter) {
  return projectService({
    url: api.batchCreateOrder,
    method: 'post',
    data: paramter
  })
}

export function getOrderPaged (parameter) {
  return projectService({
    url: api.getPaged,
    method: 'get',
    params: parameter
  })
}

export function AddDunningCount (paramter) {
  return projectService({
    url: api.AddDunningCount,
    method: 'post',
    data: paramter
  })
}

export function GetPublishCheck (parameter) {
  return FinacialAxios({
    url: api.GetPublishCheck,
    method: 'get',
    params: parameter
  })
}

export function CreatePublishCheck (paramter) {
  return FinacialAxios({
    url: api.CreatePublishCheck,
    method: 'post',
    data: paramter
  })
}

export function GetCreateOrderProjectById (parameter) {
  return projectService({
    url: api.GetCreateOrderProjectById,
    method: 'get',
    params: parameter
  })
}

export function CreatePublishOrder (parameter) {
  return FinacialAxios({
    url: api.CreatePublishOrder,
    method: 'post',
    data: parameter
  })
}

export function GetPublishOrder (parameter) {
  return FinacialAxios({
    url: api.GetPublishOrder,
    method: 'get',
    params: parameter
  })
}

export function ModifyPublishChargesOrder (paramter) {
  return FinacialAxios({
    url: api.ModifyPublishChargesOrder,
    method: 'post',
    data: paramter
  })
}

export function UpdatePublishOrder (paramter) {
  return FinacialAxios({
    url: api.UpdatePublishOrder,
    method: 'put',
    data: paramter
  })
}

export function BatchDeleteProjectOrder (parameter) {
  return projectService({
    url: api.BatchDeleteProjectOrder,
    method: 'post',
    data: parameter
  })
}

export function RemovePublishOrderByNo (parameter) {
  return FinacialAxios({
    url: api.RemovePublishOrderByNo,
    method: 'delete',
    params: parameter
  })
}
