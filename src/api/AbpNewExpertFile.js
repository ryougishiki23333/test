// 专家表-专家管理
import { zjkService as zjkAxios } from '@/lib/axios'

const api = {
  BatchCreateOrUpdate: '/api/services/app/AbpNewExpertFile/BatchCreateOrUpdate',
  GetPaged: '/api/services/app/AbpNewExpertFile/GetPaged',
  Delete: '/api/services/app/AbpNewExpertFile/Delete'
}

export function ExpertFileBatchCreateOrUpdate (params) {
  return zjkAxios({
    url: api.BatchCreateOrUpdate,
    method: 'post',
    data: params
  })
}

export function ExpertFileGetPaged (params) {
  return zjkAxios({
    url: api.GetPaged,
    method: 'get',
    params
  })
}

export function ExpertFileDelete (params) {
  return zjkAxios({
    url: api.Delete,
    method: 'delete',
    params
  })
}
