// 关注项目
import { ProjectService as axios } from '@/lib/axios'

const api = {
  GetPaged: '/api/services/app/SProjectFollow/GetPaged', // 根据当前用户获取的关注项目的分页列表信息
  CreateOrUpdate: '/api/services/app/SProjectFollow/CreateOrUpdate', // 添加或者修改的公共方法
  DeleteByProjectId: '/api/services/app/SProjectFollow/DeleteByProjectId' // 删除信息
}

export function SProjectFollowGetPaged (params) {
  return axios({
    url: api.GetPaged,
    method: 'get',
    params
  })
}
export function SProjectFollowCreateOrUpdate (params) {
  return axios({
    url: api.CreateOrUpdate,
    method: 'post',
    data: params
  })
}
export function SProjectFollowDeleteByProjectId (params) {
  return axios({
    url: api.DeleteByProjectId,
    method: 'delete',
    params
  })
}
