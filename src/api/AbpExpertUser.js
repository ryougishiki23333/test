// 5001
import { userCenterService as userCenterAxios } from '@/lib/axios'

const api = {
  GetByUserId: '/api/services/app/AbpExpertUser/GetByUserId' // 通过指定用户id获取专家基础信息
}

export function AbpExpertUserGetByUserId (params) {
  return userCenterAxios({
    url: api.GetByUserId,
    method: 'get',
    params
  })
}
