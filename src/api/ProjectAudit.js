// 审核
import { ProjectService as axios } from '@/lib/axios'

const api = {
  createOrUpdate: '/api/services/app/ProjectAudit/CreateOrUpdate',
  getPaged: '/api/services/app/ProjectAudit/GetPaged',
  GetSprojectAuditList: '/api/services/app/ProjectAudit/GetSprojectAuditList',
  Delete: '/api/services/app/ProjectAudit/Delete',
  GetCurrentLYScores: '/api/services/app/ProjectAudit/GetCurrentLYScores'
}

export function createAudit (paramter) {
  return axios({
    url: api.createOrUpdate,
    method: 'post',
    data: paramter
  })
}

export function getAudit (paramter) {
  return axios({
    url: api.getPaged,
    method: 'get',
    params: {
      ...paramter,
      MaxResultCount: 1000, //  不翻页
      SkipCount: 0
    }
  })
}

export function getByPaged (params) {
  return axios({
    url: api.GetSprojectAuditList,
    method: 'get',
    params
  })
}

export function ProjectAuditDelete (params) {
  return axios({
    url: api.Delete,
    method: 'delete',
    params
  })
}

// 接收Array，参数需为小写的companyId(可以传多个，逗号分隔)
export function ProjectAuditGetCurrentLYScores (params) {
  return axios({
    url: api.GetCurrentLYScores,
    method: 'get',
    params
  })
}