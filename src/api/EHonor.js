import { ProjectService as axios } from '@/lib/axios'

const api = {
  CreateOrUpdate: '/api/services/app/EHonor/CreateOrUpdate',
  BatchCreateOrUpdate: '/api/services/app/EHonor/BatchCreateOrUpdate', // 批量新增或修改Ehonor
  DeleteEhonor: '/api/services/app/EHonor/Delete', // 删除Ehonor信息
  GetPaged: '/api/services/app/EHonor/GetPaged' // 获取EHonor的分页列表信息
}

export function CreateOrUpdate (params) {
  return axios({
    url: api.CreateOrUpdate,
    method: 'post',
    data: params
  })
}

// 批量新增或修改需求及协议
export function BatchCreateOrUpdate (params) {
  return axios({
    url: api.BatchCreateOrUpdate,
    method: 'post',
    data: params
  })
}

// 删除需求及协议
export function DeleteEhonor (params) {
  return axios({
    url: api.DeleteEhonor,
    method: 'delete',
    params: params
  })
}

// 获取EHonor的分页列表信息
export function EhonorGetPaged (params) {
  return axios({
    url: api.GetPaged,
    method: 'get',
    params: params
  })
}
