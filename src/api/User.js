import { USER_TYPE, TENANT_ID } from "@/store/mutation-types";
import { lsOptions } from "@/lib/util";
import {
  PlatformService,
  userCenterService,
  ProjectService,
} from "@/lib/axios";
import Cookies from "js-cookie";

// 此js最先加载
function getQuery() {
  if (!location.search) {
    return {};
  } else {
    const query = location.search.split("?")[1];
    const queryArr = query.split("&");
    const result = {};
    queryArr.forEach((q) => {
      result[q.split("=")[0]] = q.split("=")[1];
    });
    return result;
  }
}

const whiteList = ["/login", "/mLogin", "/sysu/login"];
const query = getQuery();
const { token, state, mToken, mTid } = query;
if (token || mToken || whiteList.indexOf(location.pathname) > -1) {
  let value = null;
  let tenantId = null;
  if (token || location.pathname !== "/mLogin") {
    value = "userCenter";
    tenantId = state;
  }
  if (mToken || location.pathname === "/mLogin") {
    value = "medical";
    tenantId = mTid;
  }
  if (location.pathname === "/sysu/login") {
    tenantId = 2198;
  }
  localStorage.setItem(
    `${lsOptions.namespace}${USER_TYPE}`,
    JSON.stringify({
      expire: null,
      value,
    })
  );
  localStorage.setItem(
    `${lsOptions.namespace}${TENANT_ID}`,
    JSON.stringify({
      expire: null,
      value: tenantId || Cookies.get("TENANT_ID"),
    })
  );
  Cookies.set("USER_TYPE", value, { expires: 7 });
  Cookies.set("TENANT_ID", tenantId, { expires: 7 });
}

let userType = "medical";
if (localStorage.getItem(`${lsOptions.namespace}${USER_TYPE}`)) {
  const type = JSON.parse(
    localStorage.getItem(`${lsOptions.namespace}${USER_TYPE}`)
  );
  userType = type.value;
}

export const aAxios =
  userType === "medical" ? ProjectService : userCenterService;
export const bAxios =
  userType === "medical" ? ProjectService : PlatformService;

// 获取平台用户信息
export async function getInfo() {
  return await bAxios({
    url: "/api/services/app/Session/GetCurrentLoginInformations",
    method: "get",
    headers: {
      "Content-Type": "application/json;charset=UTF-8",
    },
  });
}

// 获取A用户用户信息
export function getUserAInfo() {
  return aAxios({
    url: "/api/services/app/Session/GetCurrentLoginInformations",
    method: "get",
    headers: {
      "Content-Type": "application/json;charset=UTF-8",
    },
  });
}

export function getCompanyById(companyId) {
  return aAxios({
    url: "/api/services/app/AbpCompany/GetById",
    method: "get",
    params: {
      id: companyId,
    },
  });
}

export const userCenterApi = {
  GetAll: "/api/services/app/User/GetAll",
  GetPaged: "/sqlite/productData/userAudit/filter/getPaged",
};

// export function addUserAudit(msg) {
//   return aAxios({
//     url: "/sqlite/productData/userAudit/insert",
//     method: "post",
//     params: {
//       auditStatus: "1",
//       companyId: msg.compangId,
//       companyName: "1",
//       reason: "1",
//       remark: "1",
//       tenantId: msg.id,
//       type: "sysu",
//       userId: msg.id,
//       userName: msg.userName,
//     },
//   });
// }
