const path = require('path');
const AntdDayjsWebpackPlugin = require('antd-dayjs-webpack-plugin')
const ModuleFederationPlugin = require('webpack').container.ModuleFederationPlugin
const { ifGlobalUrl } = require('./src/globalConfig')

let federationUrl;
if (ifGlobalUrl) {
  federationUrl = 'window.urlConfig.sources.federationUrl + "/federationModule.js"'
} else {
  federationUrl = process.env.NODE_ENV === 'production' ? '"https://qy.choicelink.cn:8312/federationModule.js"' : '"https://qy.choicelink.cn:8312/federationModule.js"'
}

module.exports = {
  lintOnSave: false,
  filenameHashing: false,
  css: {
    extract: true,
    loaderOptions: {
      less: {
        modifyVars: {
          /* less 变量覆盖，用于自定义 ant design 主题 */
          'primary-color': '#FF8800',
          'tabs-title-font-size-lg': '18px',
          'rate-star-color': '#FF8800'
        },
        javascriptEnabled: true
      }
    }
  },
  configureWebpack: {
    resolve: {
      extensions: ['.js', '.json', '.vue'],
      alias: {
        '@': path.resolve(__dirname, 'src/'),
      },
    },
    output: {
      library: 'icestark-vue-demo',
      libraryTarget: 'umd',
    },
    plugins: [
      new AntdDayjsWebpackPlugin({ preset: 'antdv3' }),
      new ModuleFederationPlugin({
        name: "wpExpertLibrary",
        // remotes: {
        //   federationModule: "federationModule@http://localhost:9001/federationModule.js"
        // },
        remotes: {
          federationModule: `promise new Promise(resolve => {
              const urlParams = new URLSearchParams(window.location.search)
              const version = urlParams.get('app1VersionParam')
              // This part depends on how you plan on hosting and versioning your federated modules
              const remoteUrlWithVersion = ${federationUrl}
              const script = document.createElement('script')
              script.src = remoteUrlWithVersion
              script.onload = () => {
                // the injected script has loaded and is available on window
                // we can now resolve this Promise
                const proxy = {
                  get: (request) => window.federationModule.get(request),
                  init: (arg) => {
                    try {
                      return window.federationModule.init(arg)
                    } catch(e) {
                      console.log('remote container already initialized')
                    }
                  }
                }
                resolve(proxy)
              }
              // inject this script with the src set to the versioned remoteEntry.js
              document.head.appendChild(script);
            })
          `,
        },
        shared: {
          "ant-design-vue": { singleton: true },
          // "element-ui": { singleton: true },
        }
      }),
    ],
  },
  chainWebpack: (config) => {
    config.optimization.delete('splitChunks');
    config.resolve.alias.set('vue$', 'vue/dist/vue.esm.js');
  },
  devServer: {
    port: 6004,
    headers: {
      'Access-Control-Allow-Origin': '*'
    }
  },
};
